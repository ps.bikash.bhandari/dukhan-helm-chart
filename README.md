This is for Barwa project 

# Run a Postgres Database

docker run -itd --name postgres -e  POSTGRES_HOST_AUTH_METHOD=trust --restart always -p 5432:5432 postgres:12.1



# Dockerfile

```C#

FROM progressoft/tomcat-sql5:alpine8-latest
ARG  PSCI_VERSION=1.0
ENV  VERSION=${PSCI_VERSION}
USER root

RUN apk --update add postgresql-client; \
    printf "║ %-25s │ %-25s ║\n" "barwa-ui" ${VERSION} >> /VERSION_INFO; \
    chown -R nobody:nobody /VERSION_INFO;

COPY --chown=nobody:nobody ./barwa-war-*.war            /usr/local/tomcat/webapps/core.war
COPY --chown=nobody:nobody ./barwa-ui-entrypoint.sh  /usr/local/tomcat/
RUN unzip -q /usr/local/tomcat/webapps/core.war -d         /usr/local/tomcat/webapps/ROOT \
    && cp /usr/local/tomcat/webapps/ROOT/WEB-INF/lib/keystore.jks /usr/local/tomcat/bin \
    && tar -xzvf /usr/local/tomcat/webapps/ROOT/WEB-INF/backup/app/core.tar.gz -C /usr/local/tomcat/webapps/ROOT/WEB-INF/backup/app/ \
    && rm /usr/local/tomcat/webapps/core.war \
    && chown -R nobody:nobody /usr/local/tomcat \
    && chmod +x /usr/local/tomcat/*.sh \
    && cp -r /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/liquibase/scripts /liquibase/scripts


WORKDIR /usr/local/tomcat/bin
CMD   ../barwa-ui-entrypoint.sh
USER  nobody
```
```C#
docker build -t harbor.progressoft.io/quantum/barwa-barwa-ui:1.0.2 . 
``
# Push the Imaget to harbor.progresssoft.io
# Update the Image version in  global value.yaml file
# Run the Helm chart for DUKHAN BANK
# Update the API URL in the Sysconfigs table
```C#
update mpay_sysconfigs set configvalue='http://10.152.183.2:8080/processMessage?token=y' where configkey='Spring Service Url';
```
