FROM progressoft/dbtool:latest
ARG  PSCI_VERSION=1.0
ENV  VERSION=${PSCI_VERSION}

RUN  printf "║ %-25s │ %-25s ║\n" "mpay-apis" ${VERSION} >> /VERSION_INFO; \
     chown -R nobody:nobody /VERSION_INFO

COPY --chown=nobody:nobody artifacts/barwa-apis-*.jar                 /app/mpay.jar
COPY --chown=nobody:nobody cicd/docker/barwa-apis-entrypoint.sh       /app/barwa-apis-entrypoint.sh
RUN  chmod +x /app/*.sh

WORKDIR /app
CMD     /app/barwa-apis-entrypoint.sh
USER    nobody