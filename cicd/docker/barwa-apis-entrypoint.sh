#!/bin/bash
sleep 100;
if ! dbtool $db_engine \
  --user $db_user \
  --pass $db_password \
  --host $db_host \
  --port $db_port \
  --inst $db_instance \
  --select "SELECT configkey FROM $db_user.MPAY_SYSCONFIGS WHERE CONFIGKEY='QCB Bank Code'"; then
  echo "[JFW_ENTRY] Failed to dbtool"
  exit 1
fi

cd /app || exit
java -Xmx1700m -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=7000 -jar /app/mpay.jar
