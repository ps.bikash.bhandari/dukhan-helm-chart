#!/bin/bash

if ! dbtool  $db_engine \
    --user   $db_user \
    --pass   $db_password \
    --host   $db_host \
    --port   $db_port \
    --inst   $db_instance \
    --test   MPAY_Transactions \
    --external $db_external \
    --rebuild true \
    --command "sh -c \"PGPASSWORD=$db_password psql -f /usr/local/tomcat/webapps/ROOT/WEB-INF/backup/app/corepg.dmp -h $db_host -p $db_port -U $db_user -d postgres -v ON_ERROR_STOP=1\""
    # --command "sh -c \"cd /usr/local/tomcat/webapps/ROOT/WEB-INF/lib && java -jar initializer-*.jar entities-*.jar\""
    # --command "imp system/oracle@$db_host:$db_port/$db_instance fromuser=orgdump touser=$db_user file=/usr/local/tomcat/webapps/ROOT/WEB-INF/backup/app/core.dmp ignore=yes statistics=none"

then
    echo "[JFW_ENTRY] Failed to dbtoolw"
    exit 1
fi

if [ "$liquibase_enabled" = true ] ; then
    echo "|-------------------------------------------------------|"
    echo "|       Executing database migration scripts            |"
    echo "|-------------------------------------------------------|"
    cat /liquibase/scripts/liquibase-master.xml | grep include
    if [ "$db_engine" = "postgres" ]; then
        cd /liquibase; ./liquibase --driver=$db_driver --classpath=/usr/local/tomcat/webapps/ROOT/WEB-INF/lib/postgresql-42.2.1.jar --url=$db_url --username=$db_user --password=$db_password --changeLogFile=./scripts/liquibase-master.xml update
    else
        cd /liquibase; ./liquibase --driver=$db_driver --url=$db_url --username=$db_user --password=$db_password --changeLogFile=./scripts/liquibase-master.xml update
    fi
    if [ "$?" != "0" ]; then echo "Executing migration scripts failed"; exit 1; fi;
    echo "|-------------------------------------------------------|"
    echo "|       Done executing migration scripts                |"
    echo "|-------------------------------------------------------|"
fi

sed -i 's/SYSTEM/BARWA/g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/app/camel/routes/MPClearIntegration/*
sed -i 's/tenantId=.*"/tenantId=BARWA"/g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/app/camel/routes/MPClearIntegration/mpayAutoLogin.xml
sed -i 's/default.tenant=SYSTEM/default.tenant=BARWA/g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/core/system-generic.properties
sed -i 's|7777/BARWA|7070|g' /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/config/app/camel/app-camel-settings.properties

cd /usr/local/tomcat/bin || exit
catalina.sh jpda run

