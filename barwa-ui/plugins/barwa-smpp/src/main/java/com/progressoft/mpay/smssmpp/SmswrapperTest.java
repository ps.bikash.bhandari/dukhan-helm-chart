package com.progressoft.mpay.smssmpp;

import org.jsmpp.InvalidResponseException;
import org.jsmpp.PDUException;
import org.jsmpp.bean.*;
import org.jsmpp.extra.NegativeResponseException;
import org.jsmpp.extra.ResponseTimeoutException;
import org.jsmpp.extra.SessionState;
import org.jsmpp.session.BindParameter;
import org.jsmpp.session.SMPPSession;

import java.io.IOException;

public class SmswrapperTest {

	public String SendMessage(String SMPPHOST, int SMPPPORT,
			String SMPPSYSTEMID, String PASSWORD, String SYSTEMTYPE,
			String SOURCEADDRESS, String TON, String Number, String Contents)
			throws Exception {

		String messageID = null;
		TypeOfNumber ton = null;

		SMPPSession session = new SMPPSession();

		if (Number.length() == 0 || Number.isEmpty()) {
			throw new Exception("Invalid MobileNumber");
		}

		try {
			if (session.getSessionState() == SessionState.CLOSED) {
				// logger.info("SMPP Session Closed Trying to reconnect and bind");
				session.connectAndBind(SMPPHOST, SMPPPORT, new BindParameter(
						BindType.BIND_TX, SMPPSYSTEMID, PASSWORD, SYSTEMTYPE,
						TypeOfNumber.UNKNOWN, NumberingPlanIndicator.UNKNOWN,
						null));
			}
		} catch (IOException e) {
			// logger.error(e.getMessage());
			e.printStackTrace();
		}

		if (TON.equalsIgnoreCase("INTERNATIONAL"))
			ton = TypeOfNumber.INTERNATIONAL;
		else if (TON.equalsIgnoreCase("ALPHANUMERIC")) {
			ton = TypeOfNumber.ALPHANUMERIC;
		}
		try {
			messageID = session.submitShortMessage("CMT", ton,
					NumberingPlanIndicator.UNKNOWN, SOURCEADDRESS,
					TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.ISDN,
					Number, new ESMClass(), (byte) 0, (byte) 1, null, null,
					new RegisteredDelivery(SMSCDeliveryReceipt.DEFAULT),
					(byte) 0, null/*new GeneralDataCoding(false, true, MessageClass.CLASS1, Alphabet.ALPHA_8_BIT)*/,
					(byte) 0, Contents.getBytes(), new OptionalParameter[0]);
		} catch (PDUException e) {
			// Invalid PDU parameter
			// logger.error(" SMPP EXCEPTION:===> Invalid PDU parameter");
			// logger.error(e.getMessage());
		} catch (ResponseTimeoutException e) {
			// Response timeout
			// logger.error(" SMPP EXCEPTION:===> Response timeout");
			// logger.error(e.getMessage());
		} catch (InvalidResponseException e) {
			// Invalid response
			// logger.error(" SMPP EXCEPTION:===> Receive invalid respose");
			// logger.error(e.getMessage());
		} catch (NegativeResponseException e) {
			// Receiving negative response (non-zero command_status)
			// logger.error(" SMPP EXCEPTION:===> Receive negative response");
			// logger.error(e.getMessage());
		} catch (IOException e) {
			// logger.error(" SMPP EXCEPTION:===> IO error occur");
			// logger.error(e.getMessage());
		} catch (Exception e) {
			// logger.error(" SMPP EXCEPTION:===> ");
			// logger.error(e.getMessage());
			System.out.print(e.getStackTrace());
		}

		session.unbindAndClose();
		// logger.info("Message submitted, message_id is " + messageId +
		// " receiving mobile number is " + mobilenumer);

		return messageID;

	}

}