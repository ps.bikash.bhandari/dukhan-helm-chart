package com.progressoft.mpay.smssmpp;

import com.progressoft.mpay.sms.NotificationRequest;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import smsservice.context.BarwaSmsNotificationContext;
import smsservice.response.SMSNotificationResponse;
import smsservice.service.SMSNotificationService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

public class SendSms implements Processor {

    public static final String SUCCESS_MESSAGE = "SUCCESS";
    public static final String FAILED_MESSAGE = "FAIL";
    private static final Logger LOGGER = LoggerFactory.getLogger(SendSms.class);
    private static JAXBContext jaxbContext;


    private static JAXBContext getJaxbContext() throws JAXBException {
        if (jaxbContext == null)
            jaxbContext = JAXBContext.newInstance(NotificationRequest.class);
        return jaxbContext;
    }

    @Override
    public void process(Exchange exchange) throws JAXBException {
        try {
            Unmarshaller unmarshaller = getJaxbContext().createUnmarshaller();
            NotificationRequest request = getNotificationFromBody(exchange, unmarshaller);

            BarwaSmsNotificationContext context = new BarwaSmsNotificationContext();
            context.setMessage(request.getContent());
            context.setMobileNumber(request.getReciever());

            SMSNotificationResponse smsNotificationResponse = SMSNotificationService
                    .integrateWithSMSNotificationService(context);

            if (smsNotificationResponse != null && smsNotificationResponse.isSuccess() != null
                    && smsNotificationResponse.isSuccess())
                exchange.getIn().setBody(SUCCESS_MESSAGE);
            else
                exchange.getIn().setBody(FAILED_MESSAGE);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            exchange.setProperty("errorDescription", e.getMessage());
            exchange.getIn().setBody(FAILED_MESSAGE);
        }
    }

    private NotificationRequest getNotificationFromBody(Exchange exchange, Unmarshaller unmarshaller)
            throws JAXBException {
        return (NotificationRequest) unmarshaller
                .unmarshal(new StringReader(String.valueOf(exchange.getIn().getBody())));
    }
}
