package com.progressoft.mpay.smssmpp;

import org.springframework.beans.factory.annotation.Value;

public class SmppProperties {

	@Value("${port}")
	private String port;
	@Value("${host}")
	private String host;
	@Value("${systemId}")
	private String systemId;
	@Value("${connectionAuthentication}")
	private String connectionAuthentication;
	@Value("${systemType}")
	private String systemType;
	@Value("${sourceAddress}")
	private String sourceAddress;
	@Value("${sourceAddressTon}")
	private String sourceAddressTon;
	@Value("${destinationAddressTon}")
	private String destinationAddressTon;
	@Value("${serviceType}")
	private String serviceType;
	@Value("${destinationAddressNpi}")
	private String destinationAddressNpi;
	@Value("${sourceAddressNpi}")
	private String sourceAddressNpi;
	@Value("${timeout}")
	private String timeout;
	@Value("${deliveryThreadSleep}")
	private String deliveryThreadSleep;
	@Value("${dataCoding}")
	private String dataCoding;
	@Value("${encoding}")
	private String encoding;
	@Value("${segmentLength}")
	private String segmentLength;
	@Value("${encryptedNotificationTypes}")
	private String encryptedNotificationTypes;

	public String getHost() {
		return host;
	}

	public String getPort() {
		return port;
	}

	public String getSystemId() {
		return systemId;
	}

	public String getPassword() {
		return connectionAuthentication;
	}

	public String getSystemType() {
		return systemType;
	}

	public String getSourceAddress() {
		return sourceAddress;
	}

	public String getSourceAddressTon() {
		return sourceAddressTon;
	}

	public String getDestinationAddressTon() {
		return destinationAddressTon;
	}

	public String getSourceAddressNpi() {
		return sourceAddressNpi;
	}

	public String getDestinationAddressNpi() {
		return destinationAddressNpi;
	}

	public String getServiceType() {
		return serviceType;
	}

	public String getTimeout() {
		return timeout;
	}

	public String getDeliveryThreadSleep() {
		return deliveryThreadSleep;
	}

	public String getDataCoding() {
		return dataCoding;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setHost(String host) {
		this.connectionAuthentication = host;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public void setPassword(String connectionAuthentication) {
		this.connectionAuthentication = connectionAuthentication;
	}

	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}

	public void setSourceAddress(String sourceAddress) {
		this.sourceAddress = sourceAddress;
	}

	public void setSourceAddressTon(String sourceAddressTon) {
		this.sourceAddressTon = sourceAddressTon;
	}

	public void setDestinationAddressTon(String destinationAddressTon) {
		this.destinationAddressTon = destinationAddressTon;
	}

	public void setSourceAddressNpi(String sourceAddressNpi) {
		this.sourceAddressNpi = sourceAddressNpi;
	}

	public void setDestinationAddressNpi(String destinationAddressNpi) {
		this.destinationAddressNpi = destinationAddressNpi;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	public void setDeliveryThreadSleep(String deliveryThreadSleep) {
		this.deliveryThreadSleep = deliveryThreadSleep;
	}

	public void setDataCoding(String dataCoding) {
		this.dataCoding = dataCoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getSegmentLength() {
		return segmentLength;
	}

	public void setSegmentLength(String segmentLength) {
		this.segmentLength = segmentLength;
	}

	public String getEncryptedNotificationTypes() {
		return encryptedNotificationTypes;
	}

	public void setEncryptedNotificationTypes(String encryptedTypes) {
		this.encryptedNotificationTypes = encryptedTypes;
	}
}