package com.progressoft.mpay.smssmpp;

public class SmppPropertiesTest {
	//@Value("${port}")
    private String port = "2775";
    //@Value("${host}")
    private String host="127.0.0.1";
    //@Value("${systemId}")
    private String systemId="smppclient1";
    //@Value("${connectionAuthentication}")
    private String connectionAuthentication="password" ;
    //@Value("${systemType}")
    private String systemType="smppclient1";
    //@Value("${sourceAddress}")
    private String sourceAddress="smppclient1";
    //@Value("${sourceAddressTon}")
    private String sourceAddressTon="ALPHANUMERIC";
    //@Value("${destinationAddressTon}")
    private String destinationAddressTon="INTERNATIONAL";
    //@Value("${serviceType}")
    private String serviceType="CMT";
    //@Value("${destinationAddressNpi}")
    private String destinationAddressNpi="ISDN";
    //@Value("${sourceAddressNpi}")
    private String sourceAddressNpi="UNKNOWN";
    //@Value("${timeout}")
    private String timeout="60000";
    //@Value("${deliveryThreadSleep}")
    private String deliveryThreadSleep="10000";
    //@Value("${dataCoding}")
    private String dataCoding="ALPHA_8_BIT";
    //@Value("${encoding}")
    private String encoding="UTF-16BE";

    public String getHost() {return  host;}

    public String getPort() {return  port;}

    public String getSystemId() {return  systemId;}

    public String getPassword() { return connectionAuthentication; }

    public String getSystemType() {return systemType;}

    public String getSourceAddress() {return sourceAddress;}

    public String getSourceAddressTon() {return sourceAddressTon;}

    public String getDestinationAddressTon() {return destinationAddressTon;}

    public String getSourceAddressNpi() {return sourceAddressNpi;}

    public String getDestinationAddressNpi() {return destinationAddressNpi;}

    public String getServiceType() {return serviceType;}

    public String getTimeout() {return timeout;}

    public String getDeliveryThreadSleep() {return deliveryThreadSleep;}

    public String getDataCoding() {return dataCoding;}

    public String getEncoding() {return encoding;}

    public void setHost(String host) {this.connectionAuthentication = host;}

    public void setPort(String port) {this.port = port;}

    public void setSystemId(String systemId) {this.systemId = systemId;}

    public void setPassword(String connectionAuthentication) { this.connectionAuthentication = connectionAuthentication; }

    public void setSystemType(String systemType) {this.systemType = systemType;}

    public void setSourceAddress(String sourceAddress) {this.sourceAddress = sourceAddress;}

    public void setSourceAddressTon(String sourceAddressTon) {this.sourceAddressTon = sourceAddressTon;}

    public void setDestinationAddressTon(String destinationAddressTon) {this.destinationAddressTon = destinationAddressTon;}

    public void setSourceAddressNpi(String sourceAddressNpi) {this.sourceAddressNpi = sourceAddressNpi;}

    public void setDestinationAddressNpi(String destinationAddressNpi) {this.destinationAddressNpi = destinationAddressNpi;}

    public void setServiceType(String serviceType) {this.serviceType = serviceType;}

    public void setTimeout(String timeout) {this.timeout = timeout;}

    public void setDeliveryThreadSleep(String deliveryThreadSleep) {this.deliveryThreadSleep = deliveryThreadSleep;}

    public void setDataCoding(String dataCoding) {this.dataCoding = dataCoding;}

    public void setEncoding(String encoding) {this.encoding = encoding;}
}