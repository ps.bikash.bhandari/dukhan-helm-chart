package com.progressoft.mpay.smssmpp;

import com.progressoft.mpay.exceptions.MPayGenericException;
import com.progressoft.mpay.sms.NotificationResponse;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.camel.converter.jaxp.XmlConverter;
import org.w3c.dom.Element;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.List;

public class PrepareMapyNotificationResponse implements Processor {

	private static JAXBContext jaxbContext = null;

	private static JAXBContext getJaxbContext() throws JAXBException {
		if (jaxbContext == null)
			jaxbContext = JAXBContext.newInstance(NotificationResponse.class);
		return jaxbContext;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		Marshaller marshaller = getJaxbContext().createMarshaller();
		NotificationResponse response = createNotificationResponse(exchange);
		StringWriter writer = new StringWriter();
		marshaller.marshal(response, writer);
		exchange.getIn().setBody(writer.toString());
	}

	@SuppressWarnings("rawtypes")
	public static String getStringXmlFromExchange(Exchange exchange) {
		try {
			CxfPayload cxfPayload = (CxfPayload) exchange.getIn().getBody();
			List inElements = cxfPayload.getBodySources();
			Element element = new XmlConverter().toDOMElement((Source) inElements.get(0));
			StreamResult streamResult = getXMLBody(element);
			return streamResult.getWriter().toString();
		} catch (Exception e) {
			throw new MPayGenericException("Failed to getStringXmlFromExchange", e);
		}
	}

	private static StreamResult getXMLBody(Element exDOM) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		StreamResult result = new StreamResult(new StringWriter());
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(exDOM);
		transformer.transform(source, result);
		return result;
	}

	private NotificationResponse createNotificationResponse(Exchange exchange) {
		String result = (String) exchange.getIn().getBody();
		NotificationResponse notificationResponse = new NotificationResponse();
		notificationResponse.setStatus(result);
		if ("FAIL".equals(result)) {
			notificationResponse.setErrorDescription((String) exchange.getProperty("errorDescription"));
		}
		return notificationResponse;
	}
}