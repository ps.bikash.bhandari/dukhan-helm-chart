package smsservice.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.progressoft.mpay.DataProvider;
import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.banksintegration.BankIntegrationStatus;
import com.progressoft.mpay.entities.MPAY_BankIntegMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smsservice.CreateSMSNotificationMessage;
import smsservice.context.BarwaSmsNotificationContext;
import smsservice.response.SMSNotificationResponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class SMSNotificationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SMSNotificationService.class);

    public static SMSNotificationResponse integrateWithSMSNotificationService(
            BarwaSmsNotificationContext context) {
        LOGGER.info("Inside integrateWithSMSNotificationService...");
        MPAY_BankIntegMessage bankIntegrationMessage = CreateSMSNotificationMessage
                .createBankIntegrationMessage(context);
        if (bankIntegrationMessage != null) {

            StringBuilder responseBuilder;
            try {
                responseBuilder = sendRequest(context);
            } catch (Exception e) {
                LOGGER.error("Inside send request ... " + e.getMessage());
                bankIntegrationMessage
                        .setRefStatus(LookupsLoader.getInstance().getBankIntegStatus(BankIntegrationStatus.FAILED));
                bankIntegrationMessage.setStatusDescription(e.getMessage());
                DataProvider.instance().updateBankIntegMessage(bankIntegrationMessage);
                return null;
            }

            SMSNotificationResponse smsNotificationResponse = new Gson().fromJson(responseBuilder.toString(),
                    SMSNotificationResponse.class);
            bankIntegrationMessage.setResponseContent(smsNotificationResponse.toString());
            bankIntegrationMessage.setResponseID(bankIntegrationMessage.getRequestID());
            bankIntegrationMessage.setResponseDate(SystemHelper.getSystemTimestamp());
            if (smsNotificationResponse.isSuccess() != null && smsNotificationResponse.isSuccess())
                bankIntegrationMessage
                        .setRefStatus(LookupsLoader.getInstance().getBankIntegStatus(BankIntegrationStatus.ACCEPTED));
            else
                bankIntegrationMessage
                        .setRefStatus(LookupsLoader.getInstance().getBankIntegStatus(BankIntegrationStatus.REJECTED));

            DataProvider.instance().updateBankIntegMessage(bankIntegrationMessage);
            return smsNotificationResponse;
        } else
            return null;
    }

    private static StringBuilder sendRequest(BarwaSmsNotificationContext context) throws Exception {
        // TODO: ١٨‏/١١‏/٢٠٢٠ edit for barwa
        SMSNotificationRequest smsNotificationRequest = new SMSNotificationRequest();
        smsNotificationRequest.setMessage(context.getMessage());
        smsNotificationRequest.setMobileNumber(context.getMobileNumber());
        StringBuilder responseBuilder = new StringBuilder();
        HttpURLConnection conn = (HttpURLConnection) new URL(SystemParameters.getInstance().getSmsServiceUrl()).openConnection();
        conn.setDoOutput(true);
        conn.setRequestProperty("accept-charset", "UTF-8");
        conn.setRequestProperty("content-Type", "application/json");
        conn.setRequestProperty("api_key", SystemParameters.getInstance().getAppKey());

        OutputStream os = conn.getOutputStream();
        os.write(smsNotificationRequest.toString().getBytes());
        os.flush();
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = br.readLine()) != null)
            responseBuilder.append(line);
        conn.disconnect();
        LOGGER.info("Inside integrateWithSMSNotificationService (Response) ... " + responseBuilder.toString());
        return responseBuilder;
    }


    static class SMSNotificationRequest {
        @SerializedName("mobileNumber")
        private String mobileNumber;
        @SerializedName("message")
        private String message;

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return new GsonBuilder().create().toJson(this);
        }
    }

}
