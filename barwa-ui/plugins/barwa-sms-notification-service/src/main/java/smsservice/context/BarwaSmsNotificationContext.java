package smsservice.context;

import com.google.gson.GsonBuilder;

public class BarwaSmsNotificationContext {

	private String mobileNumber;

	private String message;

	public String getMessage() {
		return message;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Override
	public String toString() {
		return new GsonBuilder().create().toJson(this);
	}
}
