package smsservice.response;

import com.google.gson.GsonBuilder;

public class SMSNotificationResponse {

	private String transactionId;

	private Boolean success;

	public Boolean isSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return new GsonBuilder().create().toJson(this);
	}
}
