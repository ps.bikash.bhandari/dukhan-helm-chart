package smsservice;

import com.progressoft.mpay.LookupsLoader;
import com.progressoft.mpay.SystemHelper;
import com.progressoft.mpay.SystemParameters;
import com.progressoft.mpay.entities.MPAY_BankIntegMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smsservice.context.BarwaSmsNotificationContext;
import smsservice.util.GenerateRand13Digits;

public class CreateSMSNotificationMessage {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSMSNotificationMessage.class);

    public static MPAY_BankIntegMessage createBankIntegrationMessage(BarwaSmsNotificationContext context) {
        try {
            LOGGER.debug("CreateMessage==========");

            MPAY_BankIntegMessage message = new MPAY_BankIntegMessage();
            message.setRequestID(GenerateRand13Digits.unique13digits());
            message.setRequestDate(SystemHelper.getSystemTimestamp());
            message.setRequestContent(context.toString());
            message.setRefTransaction(null);
            message.setIsReversal(false);
            message.setIsReversed(false);
            message.setTenantId(SystemParameters.getInstance().getTenantName());
            message.setOrgId(Long.parseLong(SystemParameters.getInstance().getOrgId()));
            message.setRefBank(LookupsLoader.getInstance().getBankByCode(SystemParameters.getInstance().getDefaultBankCode()));
            message.setMessageIntegType("0");
            com.progressoft.mpay.DataProvider.instance().persistBankIntegMessage(message);
            return message;
        } catch (Exception ex) {
            LOGGER.error("Error when CreateMessage in createBankIntegrationMessage", ex);
        }
        return null;
    }
}
