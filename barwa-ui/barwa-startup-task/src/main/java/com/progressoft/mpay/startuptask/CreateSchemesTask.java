package com.progressoft.mpay.startuptask;

import com.progressoft.jfw.shared.exception.InvalidActionException;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.JfwHelper;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.entity.MPAYView;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.logging.Logger;

@Component
public class CreateSchemesTask extends StartupTask {
    public static final String COMMISSIONS_PROCESSOR = "com.progressoft.mpay.plugins.commissionsprocessors.DefaultCommissionsProcessor";
    private static final Logger LOGGER = Logger.getLogger(CreateSchemesTask.class.getName());
    private static final String JOD = "JOD";
    private static final String DEFAULT_VALUE = "Default";

    @Override
    public void execute(OptionsProvider optionsProvider) throws InvalidActionException {
        LOGGER.info("inside execute in CreateInitialEntitiesTask");
        ILookupsLoader lookupsLoader = MPayContext.getInstance().getLookupsLoader();
        IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();

        if (dataProvider.getTaxSchemeByCode(DEFAULT_VALUE)!= null) {
            LOGGER.info("all CreateSchemesTask is already created");
            return;
        }

        MPAY_LimitsScheme limitsScheme = new MPAY_LimitsScheme();
        limitsScheme.setIsActive(true);
        limitsScheme.setCode(DEFAULT_VALUE);
        limitsScheme.setName(DEFAULT_VALUE);
        limitsScheme.setPinlessAmount(new BigDecimal(0));
        limitsScheme.setWalletCap(new BigDecimal(0));
        limitsScheme.setCurrency(lookupsLoader.getCurrency(JOD));
        limitsScheme.setDescription(DEFAULT_VALUE);
        limitsScheme.setStatusId(dataProvider.getWorkflowStatus("102105"));


        MPAY_ChargesScheme chargesScheme = new MPAY_ChargesScheme();
        chargesScheme.setCode(DEFAULT_VALUE);
        chargesScheme.setName(DEFAULT_VALUE);
        chargesScheme.setDescription(DEFAULT_VALUE);
        chargesScheme.setIsActive(true);
        chargesScheme.setCurrency(lookupsLoader.getCurrency(JOD));
        chargesScheme.setStatusId(dataProvider.getWorkflowStatus("101805"));

        MPAY_RequestTypesScheme requestTypesScheme = new MPAY_RequestTypesScheme();
        requestTypesScheme.setCode(DEFAULT_VALUE);
        requestTypesScheme.setName(DEFAULT_VALUE);
        requestTypesScheme.setDescription(DEFAULT_VALUE);
        requestTypesScheme.setStatusId(dataProvider.getWorkflowStatus("904605"));


        MPAY_CommissionScheme commissionScheme = new MPAY_CommissionScheme();
        commissionScheme.setCode(DEFAULT_VALUE);
        commissionScheme.setName(DEFAULT_VALUE);
        commissionScheme.setProcessor(COMMISSIONS_PROCESSOR);
        commissionScheme.setIsActive(true);
        commissionScheme.setDescription(DEFAULT_VALUE);
        commissionScheme.setStatusId(dataProvider.getWorkflowStatus("211805"));

        MPAY_TaxScheme taxScheme = new MPAY_TaxScheme();
        taxScheme.setName(DEFAULT_VALUE);
        taxScheme.setCode(DEFAULT_VALUE);
        taxScheme.setIsActive(true);
        taxScheme.setCurrency(lookupsLoader.getCurrency(JOD));
        taxScheme.setDescription(DEFAULT_VALUE);
        taxScheme.setStatusId(dataProvider.getWorkflowStatus("311805"));


        JfwHelper.createEntity(MPAYView.LIMITSSCHEME,limitsScheme);
        LOGGER.info("create limitsScheme successfully");
        JfwHelper.createEntity(MPAYView.CHARGESCHEME,chargesScheme);
        LOGGER.info("create chargesScheme successfully");
        JfwHelper.createEntity(MPAYView.REQUESTTYPESCHEME,requestTypesScheme);
        LOGGER.info("create requestTypesScheme successfully");
        JfwHelper.createEntity(MPAYView.COMMISIONSCHEME,commissionScheme);
        LOGGER.info("create commissionScheme successfully");
        JfwHelper.createEntity(MPAYView.TAXSCHEME,taxScheme);
        LOGGER.info("create taxScheme successfully");

    }


}

