package com.progressoft.mpay.startuptask;

import com.progressoft.jfw.model.bussinessobject.workflow.WorkflowStatus;
import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.MPAY_ServicesCategory;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

import static com.progressoft.mpay.Constants.GENERIC_ACTIVE;

@Component
public class ActivateServiceCategoriesTask extends StartupTask {

    private static final Logger LOGGER = Logger.getLogger(ActivateServiceCategoriesTask.class.getName());
    @Override
    public void execute(OptionsProvider optionsProvider) {
        LOGGER.info("inside execute in ActivateServiceCategoriesTask");
        IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();
        WorkflowStatus workflowStatus = dataProvider.getWorkflowStatus(GENERIC_ACTIVE);

        for (MPAY_ServicesCategory category : this.itemDao.getItems(MPAY_ServicesCategory.class)) {
            if (isAlreadyActive(category))
                LOGGER.info("Service Category is already active :" + category.getName());
            else {
                category.setStatusId(workflowStatus);
                dataProvider.updateServiceCategoriesStatus(category);
            }
        }
        LOGGER.info("Service Categories has been activated successfully");
    }

    private boolean isAlreadyActive(MPAY_ServicesCategory category) {
        WorkflowStatus statusId = category.getStatusId();
        return statusId != null && statusId.getCode().equalsIgnoreCase(GENERIC_ACTIVE);
    }
}

