package com.progressoft.mpay.startuptask;

import com.progressoft.jfw.startup.OptionsProvider;
import com.progressoft.jfw.startup.StartupTask;
import com.progressoft.mpay.IDataProvider;
import com.progressoft.mpay.ILookupsLoader;
import com.progressoft.mpay.MPayContext;
import com.progressoft.mpay.entities.*;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class CreateProfileAndPSPAndBank extends StartupTask {
    public static final String COMMISSIONS_PROCESSOR = "com.progressoft.mpay.plugins.commissionsprocessors.DefaultCommissionsProcessor";
    private static final Logger LOGGER = Logger.getLogger(CreateProfileAndPSPAndBank.class.getName());
    private static final String JOD = "JOD";
    private static final String DEFAULT_VALUES = "Default";

    @Override
    public void execute(OptionsProvider optionsProvider)  {
        LOGGER.info("inside execute in CreateInitialEntitiesTask");
        ILookupsLoader lookupsLoader = MPayContext.getInstance().getLookupsLoader();
        IDataProvider dataProvider = MPayContext.getInstance().getDataProvider();

        if (lookupsLoader.getBankByCode("kha") != null) {
            LOGGER.info("all initial data is already created");
            return;
        }

        MPAY_LimitsScheme limitsScheme = dataProvider.getLimitSchemeByCode(DEFAULT_VALUES);
        MPAY_ChargesScheme chargesScheme = dataProvider.getChargeSchemeByCode(DEFAULT_VALUES);
        MPAY_CommissionScheme commissionScheme = dataProvider.getCommissionScheneByCode(DEFAULT_VALUES);
        MPAY_TaxScheme taxScheme = dataProvider.getTaxSchemeByCode(DEFAULT_VALUES);
        MPAY_RequestTypesScheme requestTypesScheme = dataProvider.getRequestTypeScheme(DEFAULT_VALUES);


        MPAY_Profile profile = new MPAY_Profile();
        profile.setName(DEFAULT_VALUES);
        profile.setCode(DEFAULT_VALUES);
        profile.setLimitsScheme(limitsScheme);
        profile.setChargesScheme(chargesScheme);
        profile.setCommissionsScheme(commissionScheme);
        profile.setTaxScheme(taxScheme);
        profile.setRequestTypesScheme(requestTypesScheme);
        profile.setDescription(DEFAULT_VALUES);
        profile.setCurrency(lookupsLoader.getCurrency(JOD));
        profile.setStatusId(dataProvider.getWorkflowStatus("101705"));

//
//        MPAY_Corporate psp = new MPAY_Corporate();
//        psp.setName("psp1");
//        psp.setDescription("psp1");
//        psp.setSectorType("1");
//        psp.setClientType(lookupsLoader.getClientType("100"));
//        psp.setRegistrationDate(new Date());
//        psp.setIdType(lookupsLoader.getIDType("3"));
//        psp.setRegistrationId("123");
//        psp.setIsActive(true);
//        psp.setPrefLang(lookupsLoader.getLanguage(1));
//        psp.setCity(lookupsLoader.getCityById(1));
//        psp.setRefCountry(lookupsLoader.getCountryByIsoCode("JOR"));
//        psp.setStatusId(dataProvider.getWorkflowStatus("101505"));
//        psp.setMaxNumberOfDevices(0L);
//
//        MPAY_PspDetail pspDetail = new MPAY_PspDetail();
//        pspDetail.setPspNationalID("123");
//        pspDetail.setStatusId(dataProvider.getWorkflowStatus("10016011"));
//        List<MPAY_PspDetail> detailList = new ArrayList<>();
//        detailList.add(pspDetail);
//        psp.setRefPSPPspDetails(detailList);

        MPAY_Bank mpayBank = new MPAY_Bank();
        mpayBank.setName(DEFAULT_VALUES);
        mpayBank.setDescription(DEFAULT_VALUES);
        mpayBank.setIsActive(true);
        mpayBank.setCode("kha");
        mpayBank.setSettlementParticipant("1");
        mpayBank.setTellerCode("TELLERJOD");
        mpayBank.setStatusId(dataProvider.getWorkflowStatus("108805"));


        MPAY_BulkPayment bulkPayment = new MPAY_BulkPayment();
        bulkPayment.setFileName("Default");
        bulkPayment.setId(1l);

        MPAY_BulkRegistration bulkRegistration = new MPAY_BulkRegistration();
        bulkRegistration.setId(1l);
        bulkRegistration.setFileName("Default");

        dataProvider.updateEntity(profile);
        LOGGER.info("create profile successfully");
//        dataProvider.updateEntity(psp);
//        LOGGER.info("create psp successfully");
//        dataProvider.updateEntity(pspDetail);
//        LOGGER.info("create pspDetail successfully");
        dataProvider.updateEntity(mpayBank);
        LOGGER.info("create mpayBank successfully");

        dataProvider.updateEntity(bulkPayment);
        LOGGER.info("create bulkPayment successfully");
        dataProvider.updateEntity(bulkRegistration);
        LOGGER.info("create bulkRegistration successfully");


    }


}

