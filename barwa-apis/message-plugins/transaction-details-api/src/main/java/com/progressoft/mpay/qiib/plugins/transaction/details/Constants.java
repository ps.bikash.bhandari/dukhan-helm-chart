package com.progressoft.mpay.barwa.plugins.transaction.details;

public class Constants {
    public static final String BALANCE_KEY = "bal";
    public static final String TRANSACTIONS_KEY = "txs";
    public static final String ENDPOINT_OPERATION_KEY = "endPointOperation";
    public static final String SENDER_MOBILE_KEY = "endPointOperation";
    public static final String SENDER_SERVICE_KEY = "endPointOperation";
    public static final String RECEIVER_MOBILE_KEY = "endPointOperation";
    public static final String RECEIVER_SERVICE_KEY = "endPointOperation";
    public static final String MESSAGE_ID_KEY = "endPointOperation";
    public static final String BALANCE_BEFORE = "balanceBefore";
    public static final String BALANCE_AFTER = "balanceAfter";
}
