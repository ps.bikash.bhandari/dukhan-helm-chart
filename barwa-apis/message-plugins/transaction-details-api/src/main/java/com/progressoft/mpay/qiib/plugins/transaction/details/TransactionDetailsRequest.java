package com.progressoft.mpay.barwa.plugins.transaction.details;

import com.progressoft.mpay.common.OriginatorTypes;
import com.progressoft.mpay.entities.CoreRequest;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NullArgumentException;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TransactionDetailsRequest extends CoreRequest {
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String TX_SENDER_MOBILE_KEY = "txSenderMobile";
    private static final String TX_RECEIVER_MOBILE_KEY = "txReceiverMobile";
    private static final String TX_SENDER_SERVICE_KEY = "txSenderService";
    private static final String TX_RECEIVER_SERVICE_KEY = "txReceiverService";
    private static final String TX_TYPE_KEY = "txType";
    private static final String TX_DIRECTION_KEY = "txDirection";
    private static final String TX_OPERATION_KEY = "txOperation";
    private static final String TX_REFERENCE_KEY = "txReference";
    private static final String FROM_TIME_KEY = "fromTime";
    private static final String TO_TIME_KEY = "toTime";
    private static final String MESSAGE_ID = "messageId";


    private String txSenderMobile;
    private String txReceiverMobile;
    private String txSenderService;
    private String txReceiverService;
    private Long txTypeId;
    private Long txDirectionId;
    private String txOperation;
    private String txReference;
    private Timestamp fromTime;
    private Timestamp toTime;
    private String messageId;

    public TransactionDetailsRequest() {
        // Default
    }

    public TransactionDetailsRequest(CoreRequest request) {
        if (request == null)
            throw new NullArgumentException("request");

        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setTenant(request.getTenant());
        setChecksum(request.getChecksum());
        setExtraData(request.getExtraData());
    }

    public static TransactionDetailsRequest parseMessage(CoreRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        TransactionDetailsRequest message = new TransactionDetailsRequest(request);
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(SENDER_KEY);

        if (message.getSenderType() == null || !(message.getSenderType().trim().equals(OriginatorTypes.MOBILE.getValue()) || message.getSenderType().trim().equals(OriginatorTypes.CORPORATE.getValue())))
            throw new MessageParsingException(SENDER_TYPE_KEY);

        message.setTxSenderMobile(message.getValue(TX_SENDER_MOBILE_KEY));
        message.setTxReceiverMobile(message.getValue(TX_RECEIVER_MOBILE_KEY));
        message.setTxSenderService(message.getValue(TX_SENDER_SERVICE_KEY));
        message.setTxReceiverService(message.getValue(TX_RECEIVER_SERVICE_KEY));
        message.setTxTypeId(message.getLongValue(TX_TYPE_KEY));
        message.setTxDirectionId(message.getLongValue(TX_DIRECTION_KEY));
        message.setTxOperation(message.getValue(TX_OPERATION_KEY));
        message.setTxReference(message.getValue(TX_REFERENCE_KEY));
        message.setMessageId(message.getValue(MESSAGE_ID));
        if (message.getMessageId() == null || message.getMessageId().isEmpty()) {
            message.setToTime(parseDate(message.getValue(TO_TIME_KEY), TO_TIME_KEY));
            message.setFromTime(parseDate(message.getValue(FROM_TIME_KEY), FROM_TIME_KEY));
        }
        return message;
    }

    private static Timestamp parseDate(String value, String key) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        try {
            return new Timestamp(format.parse(value).getTime());
        } catch (ParseException e) {
            throw new MessageParsingException(key, e);
        }
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getTxSenderMobile() {
        return txSenderMobile;
    }

    public void setTxSenderMobile(String txSenderMobile) {
        this.txSenderMobile = txSenderMobile;
    }

    public String getTxReceiverMobile() {
        return txReceiverMobile;
    }

    public void setTxReceiverMobile(String txReceiverMobile) {
        this.txReceiverMobile = txReceiverMobile;
    }

    public String getTxSenderService() {
        return txSenderService;
    }

    public void setTxSenderService(String txSenderService) {
        this.txSenderService = txSenderService;
    }

    public String getTxReceiverService() {
        return txReceiverService;
    }

    public void setTxReceiverService(String txReceiverService) {
        this.txReceiverService = txReceiverService;
    }

    public Long getTxTypeId() {
        return txTypeId;
    }

    public void setTxTypeId(Long txTypeId) {
        this.txTypeId = txTypeId;
    }

    public Long getTxDirectionId() {
        return txDirectionId;
    }

    public void setTxDirectionId(Long txDirectionId) {
        this.txDirectionId = txDirectionId;
    }

    public String getTxOperation() {
        return txOperation;
    }

    public void setTxOperation(String txOperation) {
        this.txOperation = txOperation;
    }

    public String getTxReference() {
        return txReference;
    }

    public void setTxReference(String txReference) {
        this.txReference = txReference;
    }

    public Timestamp getFromTime() {
        return fromTime;
    }

    public void setFromTime(Timestamp fromTime) {
        this.fromTime = fromTime;
    }

    public Timestamp getToTime() {
        return toTime;
    }

    public void setToTime(Timestamp toTime) {
        this.toTime = toTime;
    }
}