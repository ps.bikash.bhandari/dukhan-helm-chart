package com.progressoft.mpay.barwa.plugins.transaction.details;

import com.google.gson.Gson;
import com.progressoft.mpay.common.OriginatorTypes;
import com.progressoft.mpay.common.ProcessingStatuses;
import com.progressoft.mpay.common.Reasons;
import com.progressoft.mpay.common.SystemConfigurationKeys;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.MessageProcessingRequest;
import com.progressoft.mpay.usecases.messages.MessageProcessingUseCase;
import com.progressoft.mpay.usecases.messages.validators.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TransactionDetailsMessageProcessingUseCase extends MessageProcessingUseCase {

    @Override
    protected boolean loadBySessionId() {
        return false;
    }

    @Override
    protected OriginatorTypes getSenderType() {
        return OriginatorTypes.ANY;
    }

    @Override
    protected OriginatorTypes getReceiverType() {
        return OriginatorTypes.NONE;
    }

    @Override
    protected CoreRequest getCoreRequest(MessageProcessingRequest request) {
        return TransactionDetailsRequest.parseMessage(request.getCoreRequest());
    }

    @Override
    protected void preProcessRequest(MessageProcessingRequest request) {
        super.preProcessRequest(request);
        loadRequestMetaData(request);
    }

    @Override
    protected List<IValidator> getValidators(OriginatorTypes senderType) {
        List<IValidator> validators = new ArrayList<>();
        validators.add(new SenderTypeValidator(senderType));
        validators.add(new LanguageValidator());
        validators.add(new RequestTypeValidator());
        if (senderType.equals(OriginatorTypes.MOBILE))
            validators.addAll(getMobileValidators());
        else if (senderType.equals(OriginatorTypes.CORPORATE))
            validators.addAll(getCorporateValidators());
        return validators;
    }

    @Override
    protected List<NotificationMetaData> createNotifications(MessageProcessingRequest request, ProcessingStatuses processingStatusCode) {
        return new ArrayList<>();
    }

    @Override
    protected CoreResponse generateResponse(MessageProcessingRequest request, Reasons reasonCode, String reasonDescription, ProcessingStatuses processingStatus) {
        CoreResponse response = super.generateResponse(request, reasonCode, reasonDescription, processingStatus);
        if (!processingStatus.equals(ProcessingStatuses.ACCEPTED))
            return response;
        List<ExtraData> extraDataList = new ArrayList<>();
        Account account = request.getSender().getAccount();
        extraDataList.add(new ExtraData(Constants.BALANCE_KEY, account.getBalance().toString()));
        TransactionDetailsRequest message = (TransactionDetailsRequest) request.getCoreRequest();

        String txDetailsSenderName = request.getModelFactory().getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.TRANSACTION_DETAILS_SENDER_NAME.getValue(), request.getTenant()).getValue();
        List<Transaction> transactions = new ArrayList<>();
        if (message.getSender().equalsIgnoreCase(txDetailsSenderName)) {
            if (message.getMessageId() != null && !message.getMessageId().isEmpty()) {
                Long entityId = getEntityId(request, Constants.MESSAGE_ID_KEY);
                if (entityId == null) return response;
                transactions.add(request.getModelFactory().getTransactionModel().getByMessageId(entityId));
            } else
                extraDataList.add(new ExtraData(Constants.TRANSACTIONS_KEY, buildTransactionsDetails(request)));
        } else {
            Timestamp fromTime = message.getFromTime();
            Timestamp toTime = message.getToTime();
            if (fromTime == null || toTime == null)
                return response;
            if (request.getSender().getMobile() != null)
                transactions = request.getModelFactory().getTransactionModel().listMobileTransactions(request.getSender().getMobile().getId(), fromTime.toLocalDateTime(), toTime.toLocalDateTime());
            else
                transactions = request.getModelFactory().getTransactionModel().listServiceTransactions(request.getSender().getService().getId(), fromTime.toLocalDateTime(), toTime.toLocalDateTime());
        }

        if (!transactions.isEmpty())
            extraDataList.add(new ExtraData(Constants.TRANSACTIONS_KEY, toTransactionDetailsJson(request, transactions)));
        if (!transactions.isEmpty()) {
            long decimalCount = request.getModelFactory().getSystemConfigurationModel().getLong(SystemConfigurationKeys.DECIMAL_COUNT.getValue(), request.getTenant());
            Optional<JournalVoucher> optionalLastJvs = transactions.get(0).getJournalVouchers().stream()
                    .filter(jv -> request.getModelFactory().getAccountModel().get(jv.getAccountId()).getAccountNumber().equalsIgnoreCase(account.getAccountNumber()))
                    .findFirst();
            optionalLastJvs.ifPresent
                    (lastJv -> extraDataList.add(new ExtraData(Constants.BALANCE_AFTER, request.getModelFactory().getFormatterModel().formatAmount(lastJv.getBalanceAfter(), decimalCount))));

            Optional<JournalVoucher> optionalFirstJvs = transactions.get(transactions.size() - 1).getJournalVouchers().stream()
                    .filter(jv -> request.getModelFactory().getAccountModel().get(jv.getAccountId()).getAccountNumber().equalsIgnoreCase(account.getAccountNumber())).findFirst();
            optionalFirstJvs.ifPresent
                    (firstJv -> extraDataList.add(new ExtraData(Constants.BALANCE_BEFORE, request.getModelFactory().getFormatterModel().formatAmount(firstJv.getBalanceBefore(), decimalCount))));
        }
        response.setExtraData(extraDataList);
        return response;
    }

    private void loadRequestMetaData(MessageProcessingRequest request) {
        TransactionDetailsRequest message = (TransactionDetailsRequest) request.getCoreRequest();
        if (!CoreRequest.isBlank(message.getTxOperation())) {
            EndPointOperation endPointOperation = request.getModelFactory().getEndPointOperationModel().get(message.getTxOperation(), request.getTenant());
            if (endPointOperation == null)
                return;
            request.getExtraData().put(Constants.ENDPOINT_OPERATION_KEY, endPointOperation);
        }
        if (!CoreRequest.isBlank(message.getTxSenderMobile())) {
            CustomerMobile senderMobile = request.getModelFactory().getCustomerMobileModel().get(message.getTxSenderMobile(), OriginatorTypes.MOBILE, request.getTenant());
            if (senderMobile == null)
                return;
            request.getExtraData().put(Constants.SENDER_MOBILE_KEY, senderMobile);
        }
        if (!CoreRequest.isBlank(message.getTxSenderService())) {
            CorporateService senderService = request.getModelFactory().getCorporateServiceModel().get(message.getTxSenderService(), OriginatorTypes.CORPORATE, request.getTenant());
            if (senderService == null)
                return;
            request.getExtraData().put(Constants.SENDER_SERVICE_KEY, senderService);
        }
        if (!CoreRequest.isBlank(message.getTxReceiverMobile())) {
            CustomerMobile receiverMobile = request.getModelFactory().getCustomerMobileModel().get(message.getTxReceiverMobile(), OriginatorTypes.MOBILE, request.getTenant());
            if (receiverMobile == null)
                return;
            request.getExtraData().put(Constants.RECEIVER_MOBILE_KEY, receiverMobile);
        }
        if (!CoreRequest.isBlank(message.getTxReceiverService())) {
            CorporateService receiverService = request.getModelFactory().getCorporateServiceModel().get(message.getTxReceiverService(), OriginatorTypes.CORPORATE, request.getTenant());
            if (receiverService == null)
                return;
            request.getExtraData().put(Constants.RECEIVER_SERVICE_KEY, receiverService);
        }

        if (!CoreRequest.isBlank(message.getMessageId())) {
            CoreMessage coreMessage = request.getModelFactory().getCoreMessageModel().getByRequestId(message.getMessageId(), request.getTenant());
            if (coreMessage == null)
                return;
            request.getExtraData().put(Constants.MESSAGE_ID_KEY, coreMessage);
        }
    }

    private String buildTransactionsDetails(MessageProcessingRequest request) {
        List<Transaction> transactions = request.getModelFactory().getTransactionModel().find(buildFilter(request));
        if (transactions.isEmpty())
            return "";
        return toTransactionDetailsJson(request, transactions);
    }

    private TransactionDetailsFilter buildFilter(MessageProcessingRequest request) {
        TransactionDetailsRequest transactionDetailsRequest = (TransactionDetailsRequest) request.getCoreRequest();
        TransactionDetailsFilter filter = new TransactionDetailsFilter();
        filter.setTxTypeId(transactionDetailsRequest.getTxTypeId());
        filter.setTxDirectionId(transactionDetailsRequest.getTxDirectionId());
        filter.setTxReference(transactionDetailsRequest.getTxReference());
        filter.setFromTime(transactionDetailsRequest.getFromTime());
        filter.setToTime(transactionDetailsRequest.getToTime());
        filter.setMessageId(getEntityId(request, Constants.MESSAGE_ID_KEY));
        filter.setTxSenderMobileId(getEntityId(request, Constants.SENDER_MOBILE_KEY));
        filter.setTxReceiverMobile(getEntityId(request, Constants.RECEIVER_MOBILE_KEY));
        filter.setTxSenderServiceId(getEntityId(request, Constants.SENDER_SERVICE_KEY));
        filter.setTxReceiverServiceId(getEntityId(request, Constants.RECEIVER_SERVICE_KEY));
        filter.setTxOperationId(getEntityId(request, Constants.ENDPOINT_OPERATION_KEY));
        return filter;
    }

    private Long getEntityId(MessageProcessingRequest request, String key) {
        if (!request.getExtraData().containsKey(key))
            return null;
        return ((MPayEntity) request.getExtraData().get(key)).getId();
    }

    private String toTransactionDetailsJson(MessageProcessingRequest request, List<Transaction> transactions) {
        return new Gson().toJson(transactions.stream().map(tx -> new TransactionsDetailsTransaction(tx, request.getSender().getInfo(), request.getModelFactory())).collect(Collectors.toList()));
    }

    private List<IValidator> getMobileValidators() {
        List<IValidator> validators = new ArrayList<>();
        validators.add(new SenderMobileValidator());
        validators.add(new SenderCustomerValidator());
        validators.add(new SenderMobileAccountValidator());
        return validators;
    }

    private List<IValidator> getCorporateValidators() {
        List<IValidator> validators = new ArrayList<>();
        validators.add(new SenderServiceValidator());
        validators.add(new SenderCorporateValidator());
        validators.add(new SenderServiceAccountValidator());
        return validators;
    }
}
