package com.progressoft.mpay.barwa.plugins.transaction.details;

import com.progressoft.mpay.common.TransactionTypes;
import com.progressoft.mpay.entities.Currency;
import com.progressoft.mpay.entities.Transaction;
import com.progressoft.mpay.models.IModelFactory;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class TransactionsDetailsTransaction {
    // private static final String CREDIT_TYPE_CODE = "1";
    private long id;
    private double amnt;
    private double chargeAmount;
    private double taxAmount;
    private double totalAmount;
    private double externalFees;
    private String curr;
    private String sender;
    private String receiver;
    private Timestamp date;
    private String desc;
    private long status;
    private String transactionType;
    private String direction;
    private String fees;
    private String messageType;
    private long reference;
    private boolean isReversed;
    private String senderBank;
    private String receiverBank;
    private String mpClearIntegMsgLogId;
    private String statusDesc;
    private String reasonDescription;
    private String senderName;
    private String receiverName;

    public TransactionsDetailsTransaction() {
        //
    }

    public TransactionsDetailsTransaction(Transaction transaction, String sender, IModelFactory factory) {

        this.id = transaction.getId();
        this.desc = transaction.getComments();
        this.mpClearIntegMsgLogId = String.valueOf(transaction.getInwardMPClearMessageId());
        this.statusDesc = transaction.getProcessingStatus().name();
        prepareAmounts(transaction, sender, factory);
        this.curr = factory.getLookupsModel().get(transaction.getCurrencyId(), Currency.class).getStringIsoCode();
        this.reasonDescription = transaction.getReasonDescription();
        prepareSenderAndReceiver(transaction, sender);
        if (transaction.getSenderName() != null && !transaction.getSenderName().isEmpty())
            senderName = transaction.getSenderName();
        if (transaction.getReceiverName() != null && !transaction.getReceiverName().isEmpty())
            receiverName = transaction.getReceiverName();
        this.messageType = getOperationName(transaction.getOperationId(), factory);
        this.date = transaction.getDate();
        this.status = transaction.getProcessingStatus().getValue();
        this.reference = Long.parseLong(transaction.getReference());
        this.isReversed = transaction.isReversed();
        this.transactionType = transaction.getType().name();
        this.direction = transaction.getDirection().name();
        this.fees = transaction.getTotalCharge().toString();
        if (amnt >= 0) {
            totalAmount = Math.abs(totalAmount);
            fees = String.valueOf(Math.abs(new Double(fees)));
        } else if (amnt < 0) {
            double feesDouble = Double.parseDouble(fees);
            if (feesDouble != 0)
                fees = String.valueOf(feesDouble);
        }

        if (transaction.getSenderBankId() > 0)
            senderBank = factory.getBankModel().get(transaction.getSenderBankId()).getName();
        if (transaction.getReceiverBankId() > 0)
            receiverBank = factory.getBankModel().get(transaction.getReceiverBankId()).getName();
    }

    private String getOperationName(long operationId, IModelFactory modelFactory) {
        return modelFactory.getEndPointOperationModel().get(operationId).getName();
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    private void prepareSenderAndReceiver(Transaction transaction, String sender) {
        this.sender = transaction.getSenderInfo();
        this.receiver = transaction.getReceiverInfo();
    }


    private String getTransactionSender(Transaction transaction, IModelFactory modelFactory) {
        if (transaction.getSenderMobileId() == null && transaction.getSenderServiceId() == null)
            return transaction.getSenderInfo();
        else if (transaction.getSenderMobileId() == null)
            return modelFactory.getCorporateServiceModel().get(transaction.getSenderServiceId()).getName();
        else
            return modelFactory.getCustomerMobileModel().get(transaction.getSenderMobileId()).getMobileNumber();
    }

    private String getTransactionReceiver(Transaction transaction, IModelFactory modelFactory) {
        if (transaction.getReceiverMobileId() == null && transaction.getReceiverServiceId() == null)
            return transaction.getReceiverInfo();
        else if (transaction.getReceiverMobileId() == null)
            return modelFactory.getCorporateServiceModel().get(transaction.getReceiverServiceId()).getName();
        else
            return modelFactory.getCustomerMobileModel().get(transaction.getReceiverMobileId()).getMobileNumber();
    }

    private void prepareAmounts(Transaction transaction, String sender, IModelFactory factory) {
        this.amnt = transaction.getOriginalAmount().doubleValue();
        String transactionSender = getTransactionSender(transaction, factory);
        String transactionReceiver = getTransactionReceiver(transaction, factory);
        BigDecimal senderCharge = transaction.getSenderCharge() == null ? new BigDecimal(0) : transaction.getSenderCharge();
        BigDecimal senderTax = transaction.getSenderTax() == null ? new BigDecimal(0) : transaction.getSenderTax();
        BigDecimal totalAmount = transaction.getTotalAmount() == null ? new BigDecimal(0) : transaction.getTotalAmount();
        BigDecimal receiverCharge = transaction.getReceiverCharge() == null ? new BigDecimal(0) : transaction.getReceiverCharge();
        BigDecimal receiverTax = transaction.getReceiverTax() == null ? new BigDecimal(0) : transaction.getReceiverTax();
        BigDecimal externalFees = transaction.getExternalFees() == null ? new BigDecimal(0) : transaction.getExternalFees();

        if (transaction.getType().name().equals(TransactionTypes.DIRECT_CREDIT.name()))
            isTransactionCredit(transaction, sender, transactionSender, transactionReceiver,
                    senderCharge, senderTax, totalAmount, receiverCharge, receiverTax);
        else
            isTransactionDebit(transaction, sender, transactionSender, transactionReceiver,
                    senderCharge, senderTax, totalAmount, receiverCharge, receiverTax);
        if (this.externalFees > 0)
            this.externalFees = externalFees.doubleValue();
    }

    private void isTransactionDebit(Transaction transaction, String sender, String transactionSender,
                                    String transactionReceiver, BigDecimal senderCharge, BigDecimal senderTax,
                                    BigDecimal totalAmount, BigDecimal receiverCharge, BigDecimal receiverTax) {
        if (sender.equals(transactionSender)) {
            this.chargeAmount = senderCharge.doubleValue();
            this.taxAmount = senderTax.doubleValue();
            this.totalAmount = transaction.getOriginalAmount().subtract(senderCharge.add(senderTax)).doubleValue();
        } else if (sender.equals(transactionReceiver)) {
            this.chargeAmount = receiverCharge.doubleValue();
            this.taxAmount = receiverTax.doubleValue();
            this.totalAmount = totalAmount.doubleValue();
        }
    }

    private void isTransactionCredit(Transaction transaction, String sender, String transactionSender,
                                     String transactionReceiver, BigDecimal senderCharge, BigDecimal senderTax,
                                     BigDecimal totalAmount, BigDecimal receiverCharge, BigDecimal receiverTax) {
        if (sender.equals(transactionSender)) {
            this.chargeAmount = senderCharge.doubleValue();
            this.taxAmount = senderTax.doubleValue();
            this.totalAmount = totalAmount.doubleValue();
        } else if (sender.equals(transactionReceiver)) {
            this.chargeAmount = receiverCharge.doubleValue();
            this.taxAmount = receiverTax.doubleValue();
            this.totalAmount = transaction.getOriginalAmount().subtract(receiverCharge.add(receiverTax)).doubleValue();
        }
    }


    public String getMpClearIntegMsgLogId() {
        return mpClearIntegMsgLogId;
    }

    public void setMpClearIntegMsgLogId(String mpClearIntegMsgLogId) {
        this.mpClearIntegMsgLogId = mpClearIntegMsgLogId;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    long getId() {
        return id;
    }

    void setId(long id) {
        this.id = id;
    }

    double getAmnt() {
        return amnt;
    }

    void setAmnt(double amnt) {
        this.amnt = amnt;
    }

    public double getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(double chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getExternalFees() {
        return externalFees;
    }

    public void setExternalFees(double externalFees) {
        this.externalFees = externalFees;
    }

    public String getCurr() {
        return curr;
    }

    public void setCurr(String curr) {
        this.curr = curr;
    }

    String getSender() {
        return sender;
    }

    void setSender(String sender) {
        this.sender = sender;
    }

    String getReceiver() {
        return receiver;
    }

    void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    Timestamp getDate() {
        return date;
    }

    void setDate(Timestamp date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    double getReference() {
        return reference;
    }

    void setReference(long refe) {
        this.reference = refe;
    }


    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public boolean isReversed() {
        return isReversed;
    }

    public void setReversed(boolean isReversed) {
        this.isReversed = isReversed;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getSenderBank() {
        return senderBank;
    }

    public void setSenderBank(String senderBank) {
        this.senderBank = senderBank;
    }

    public String getReceiverBank() {
        return receiverBank;
    }

    public void setReceiverBank(String receiverBank) {
        this.receiverBank = receiverBank;
    }
}
