package com.progressoft.mpay.barwa.plugins.transaction.details;

import com.progressoft.mpay.common.MPClearIntegrationTypes;
import com.progressoft.mpay.usecases.messages.IMessageProcessingActionsUseCase;
import com.progressoft.mpay.usecases.messages.IMessageProcessingUseCaseFactory;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingUseCase;
import com.progressoft.mpay.usecases.messages.MessageProcessingUseCase;

public class TransactionDetailsMessageProcessingUseCaseFactory implements IMessageProcessingUseCaseFactory {
    @Override
    public MessageProcessingUseCase getMessageProcessingUseCase() {
        return new TransactionDetailsMessageProcessingUseCase();
    }

    @Override
    public IntegrationProcessingUseCase getIntegrationProcessingUseCase(MPClearIntegrationTypes type) {
        return null;
    }

    @Override
    public IMessageProcessingActionsUseCase getMessageProcessingActionsUseCase() {
        return null;
    }
}
