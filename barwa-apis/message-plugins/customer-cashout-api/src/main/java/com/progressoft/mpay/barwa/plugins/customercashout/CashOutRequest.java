package com.progressoft.mpay.barwa.plugins.customercashout;

import com.progressoft.mpay.common.OriginatorTypes;
import com.progressoft.mpay.entities.CoreRequest;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NullArgumentException;

import java.math.BigDecimal;

public class CashOutRequest extends CoreRequest {
    public static final String CHARGES_AMOUNT_KEY = "charges";
    public static final String TAX_AMOUNT_KEY = "taxes";
    private static final String RECIPIENT_NAME = "recipientName";
    private static final String BANK_ACCOUNT = "bankAccount";
    private static final String SOURCE = "source";

    private BigDecimal chargesAmount;
    private BigDecimal taxesAmount;
    private String recipientName;
    private String bankAccount;
    private String source;

    public CashOutRequest(CoreRequest request) {
        if (request == null)
            throw new NullArgumentException("request");

        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setAmount(request.getAmount());
        setReceiver(request.getReceiver());
        setReceiverType(request.getReceiverType());
        setSenderAccount(request.getSenderAccount());
        setReceiverAccount(request.getReceiverAccount());
        setExtraData(request.getExtraData());
        setTenant(request.getTenant());
        setChecksum(request.getChecksum());
        setNotes(request.getNotes());
    }

    public static CashOutRequest parseMessage(CoreRequest request) {
        if (request == null)
            return null;

        CashOutRequest message = new CashOutRequest(request);
        if (message.getSender() == null || message.getSender().length() == 0)
            throw new MessageParsingException(SENDER_KEY);

        if (message.getSenderType() == null || !message.getSenderType().trim().equals(OriginatorTypes.MOBILE.getValue()))
            throw new MessageParsingException(SENDER_TYPE_KEY);

        String charges = message.getValue(CHARGES_AMOUNT_KEY);
        if (charges == null)
            message.setChargesAmount(BigDecimal.ZERO);
        else {
            try {
                message.setChargesAmount(BigDecimal.valueOf(Double.parseDouble(charges)));
            } catch (NumberFormatException ex) {
                throw new MessageParsingException(CHARGES_AMOUNT_KEY);
            }
        }

        String taxes = message.getValue(TAX_AMOUNT_KEY);
        if (taxes == null)
            message.setTaxesAmount(BigDecimal.ZERO);
        else {
            try {
                message.setTaxesAmount(BigDecimal.valueOf(Double.parseDouble(taxes)));
            } catch (NumberFormatException ex) {
                throw new MessageParsingException(TAX_AMOUNT_KEY);
            }
        }
        message.setSource(message.getValue(SOURCE));
        message.setBankAccount(message.getValue(BANK_ACCOUNT));
        message.setRecipientName(message.getValue(RECIPIENT_NAME));

        return message;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public BigDecimal getChargesAmount() {
        return chargesAmount;
    }

    public void setChargesAmount(BigDecimal chargesAmount) {
        this.chargesAmount = chargesAmount;
    }

    public BigDecimal getTaxesAmount() {
        return taxesAmount;
    }

    public void setTaxesAmount(BigDecimal taxesAmount) {
        this.taxesAmount = taxesAmount;
    }
}
