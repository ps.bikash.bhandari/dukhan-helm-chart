package com.progressoft.mpay.barwa.plugins.customercashout;

import com.progressoft.mpay.common.MPClearIntegrationTypes;
import com.progressoft.mpay.exceptions.NotImplementedException;
import com.progressoft.mpay.usecases.messages.FinancialMessageProcessingUseCaseFactory;
import com.progressoft.mpay.usecases.messages.IMessageProcessingActionsUseCase;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingUseCase;
import com.progressoft.mpay.usecases.messages.MessageProcessingUseCase;

public class CashOutMessageProcessingUseCaseFactory extends FinancialMessageProcessingUseCaseFactory {

    @Override
    public MessageProcessingUseCase getMessageProcessingUseCase() {
        return new CashOutMessageProcessingUseCase();
    }

    @Override
    public IntegrationProcessingUseCase getIntegrationProcessingUseCase(MPClearIntegrationTypes type) {
        return new CashOutFinancialIntegrationProcessingUseCase(new CashOutResponseProcessor());
    }

    @Override
    public IMessageProcessingActionsUseCase getMessageProcessingActionsUseCase() {
        throw new NotImplementedException("getMessageProcessingActionsUseCase");
    }
}
