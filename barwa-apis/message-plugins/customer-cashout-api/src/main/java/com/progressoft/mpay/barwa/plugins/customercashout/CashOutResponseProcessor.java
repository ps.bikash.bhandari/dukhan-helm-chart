package com.progressoft.mpay.barwa.plugins.customercashout;

import com.progressoft.mpay.usecases.messages.IntegrationProcessingRequest;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingResponse;
import com.progressoft.mpay.usecases.messages.integrations.ResponseProcessor;

public class CashOutResponseProcessor extends ResponseProcessor {
    @Override
    protected void createMPClearResponse(IntegrationProcessingRequest request, IntegrationProcessingResponse response) {
    }
}