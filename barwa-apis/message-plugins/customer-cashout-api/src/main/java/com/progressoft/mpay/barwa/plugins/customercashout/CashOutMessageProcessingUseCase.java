package com.progressoft.mpay.barwa.plugins.customercashout;

import com.progressoft.mpay.common.NotificationTypes;
import com.progressoft.mpay.common.OriginatorTypes;
import com.progressoft.mpay.common.ProcessingStatuses;
import com.progressoft.mpay.entities.CoreRequest;
import com.progressoft.mpay.entities.NotificationMetaData;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.FinancialMessageProcessingUseCase;
import com.progressoft.mpay.usecases.messages.MessageProcessingRequest;
import com.progressoft.mpay.usecases.messages.MessageProcessingResponse;
import com.progressoft.mpay.usecases.messages.notifications.NotificationContext;
import com.progressoft.mpay.usecases.messages.notifications.NotificationMetaDataBuilder;
import com.progressoft.mpay.usecases.messages.validators.*;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CashOutMessageProcessingUseCase extends FinancialMessageProcessingUseCase {


    @Override
    protected boolean loadBySessionId() {
        return false;
    }

    @Override
    protected OriginatorTypes getSenderType() {
        return OriginatorTypes.MOBILE;
    }

    @Override
    protected OriginatorTypes getReceiverType() {
        return OriginatorTypes.CORPORATE;
    }

    @Override
    protected CoreRequest getCoreRequest(MessageProcessingRequest request) {
        return CashOutRequest.parseMessage(request.getCoreRequest());
    }

    @Override
    protected List<IValidator> getValidators(OriginatorTypes senderType) {
        if (senderType == null || !senderType.equals(OriginatorTypes.MOBILE))
            throw new NotSupportedException("senderType: " + Objects.requireNonNull(senderType).getValue());
        List<IValidator> validators = new ArrayList<>();
        validators.add(new LanguageValidator());
        validators.add(new RequestTypeValidator());
        validators.add(new SenderMobileValidator());
        validators.add(new SenderMobileAccountValidator());
        validators.add(new SenderCustomerValidator());
        validators.add(new ReceiverServiceValidator());
        validators.add(new ReceiverServiceAccountValidator());
        validators.add(new ReceiverCorporateValidator());
        validators.add(new TransactionConfigValidator());
        validators.add(new LimitsValidator());
        return validators;
    }

    @Override
    protected List<NotificationMetaData> createNotifications(MessageProcessingRequest request, ProcessingStatuses processingStatusCode) {
        if (processingStatusCode.equals(ProcessingStatuses.ACCEPTED))
            return new NotificationMetaDataBuilder(request, NotificationTypes.ACCEPTED_SENDER, NotificationTypes.NONE).build(NotificationContext.build(request));
        return new ArrayList<>();
    }

    @Override
    protected void preAcceptRequest(MessageProcessingRequest request) {
        super.preAcceptRequest(request);
        CashOutRequest cashOutRequest = (CashOutRequest) request.getCoreRequest();
        request.getTransaction().setCustomerBankAccountNumber(StringUtils.isEmpty(cashOutRequest.getBankAccount()) ? null : cashOutRequest.getBankAccount());
        request.getTransaction().setReceiverName(StringUtils.isEmpty(cashOutRequest.getRecipientName()) ? null : cashOutRequest.getRecipientName());
        request.getTransaction().setSenderName(request.getSender().getCustomer().getFullName());
    }

    @Override
    protected void postAcceptRequest(MessageProcessingRequest request, MessageProcessingResponse response) {
        super.postAcceptRequest(request, response);
        response.setProcessingStatus(ProcessingStatuses.ACCEPTED);
        response.getCoreMessage().setProcessingStatus(ProcessingStatuses.ACCEPTED);
        response.getTransaction().setProcessingStatus(ProcessingStatuses.ACCEPTED);
        response.getResponse().setStatusCode(String.valueOf(ProcessingStatuses.ACCEPTED.getValue()));

    }
}
