package com.progressoft.mpay.barwa.plugins.smsservice.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.progressoft.mpay.common.BankIntegrationProcessingStatuses;
import com.progressoft.mpay.common.SystemConfigurationKeys;
import com.progressoft.mpay.entities.BankIntegrationMessage;
import com.progressoft.mpay.barwa.plugins.smsservice.CreateSMSNotificationMessage;
import com.progressoft.mpay.barwa.plugins.smsservice.context.SMSNotificationContext;
import com.progressoft.mpay.barwa.plugins.smsservice.response.SMSNotificationResponse;
import com.progressoft.mpay.models.IModelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;

public class SMSNotificationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SMSNotificationService.class);

    public static SMSNotificationResponse integrate(SMSNotificationContext smsNotificationContext, IModelFactory factory) {
        LOGGER.info("Inside integrateWithSMSNotificationService...");
        BankIntegrationMessage bankIntegrationMessage = CreateSMSNotificationMessage.createBankIntegrationMessage(smsNotificationContext, factory);
        if (bankIntegrationMessage != null) {
            StringBuilder responseBuilder;
            try {
                responseBuilder = sendRequest(smsNotificationContext, factory);
            } catch (Exception e) {
                LOGGER.error("Inside send request ... " + e.getMessage());
                bankIntegrationMessage.setStatus(BankIntegrationProcessingStatuses.FAILED);
                bankIntegrationMessage.setStatusDescription(e.getMessage());
                factory.getBankIntegrationMessageModel().persist(bankIntegrationMessage);
                return null;
            }

            SMSNotificationResponse smsNotificationResponse = new Gson().fromJson(responseBuilder.toString(),
                    SMSNotificationResponse.class);
            bankIntegrationMessage.setResponseContent(smsNotificationResponse.toString());
            bankIntegrationMessage.setResponseId(bankIntegrationMessage.getRequestId());
            bankIntegrationMessage.setResponseDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
            if (smsNotificationResponse.isSuccess() != null && smsNotificationResponse.isSuccess())
                bankIntegrationMessage.setStatus(BankIntegrationProcessingStatuses.ACCEPTED);
            else
                bankIntegrationMessage.setStatus(BankIntegrationProcessingStatuses.REJECTED);

            factory.getBankIntegrationMessageModel().persist(bankIntegrationMessage);
            return smsNotificationResponse;
        } else
            return null;
    }

    private static StringBuilder sendRequest(SMSNotificationContext context, IModelFactory factory) throws Exception {
        // TODO: ١٨‏/١١‏/٢٠٢٠ for barwa
        String smsUrl = factory.getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.SMS_NOTIFICATION_SERVICE_URL.getValue()).getValue();
        String apiKey = factory.getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.API_KEY.getValue()).getValue();
        SMSNotificationRequest smsNotificationRequest = new SMSNotificationRequest();
        smsNotificationRequest.setMessage(context.getMessage());
        smsNotificationRequest.setMobileNumber(context.getMobileNumber());
        StringBuilder responseBuilder = new StringBuilder();
        HttpURLConnection conn = (HttpURLConnection) new URL(smsUrl).openConnection();
        conn.setDoOutput(true);
        conn.setRequestProperty("accept-charset", "UTF-8");
        conn.setRequestProperty("content-Type", "application/json");
        conn.setRequestProperty("api_key", apiKey);

        OutputStream os = conn.getOutputStream();
        os.write(smsNotificationRequest.toString().getBytes());
        os.flush();
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = br.readLine()) != null)
            responseBuilder.append(line);
        conn.disconnect();
        LOGGER.info("Inside integrateWithSMSNotificationService (Response) ... " + responseBuilder.toString());
        return responseBuilder;
    }


    static class SMSNotificationRequest {
        @SerializedName("mobileNumber")
        private String mobileNumber;
        @SerializedName("message")
        private String message;

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return new GsonBuilder().create().toJson(this);
        }
    }

}
