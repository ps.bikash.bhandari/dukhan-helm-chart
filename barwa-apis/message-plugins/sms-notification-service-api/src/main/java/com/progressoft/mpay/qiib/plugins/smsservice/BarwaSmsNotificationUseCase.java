package com.progressoft.mpay.barwa.plugins.smsservice;

import com.progressoft.mpay.common.MPayActiveMq;
import com.progressoft.mpay.entities.Notification;
import com.progressoft.mpay.entities.NotificationMetaData;
import com.progressoft.mpay.exceptions.GenericException;
import com.progressoft.mpay.barwa.plugins.smsservice.context.SMSNotificationContext;
import com.progressoft.mpay.usecases.notification.creator.INotificationCreatorUseCase;
import com.progressoft.mpay.usecases.notification.creator.NotificationCreatorUseCaseRequest;
import com.progressoft.mpay.usecases.notification.creator.NotificationCreatorUseCaseResponse;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class BarwaSmsNotificationUseCase implements INotificationCreatorUseCase {
    private static final String QUEUE_NAME = "mpay.barwa.sms.outward";
    private final VelocityEngine ve;

    public BarwaSmsNotificationUseCase() {
        try {
            Properties properties = new Properties();
            InputStream stream = getClass().getClassLoader().getResourceAsStream("velocity.properties");
            if (stream == null)
                throw new FileNotFoundException("velocity.properties not found in resources");
            properties.load(stream);
            ve = new VelocityEngine(properties);
            ve.init();
        } catch (Exception e) {
            throw new GenericException("Error while initializing VelocityNotificationUseCase", e);
        }
    }

    private static boolean isProbablyArabic(String s) {
        for (int i = 0; i < s.length(); ) {
            int c = s.codePointAt(i);
            if (c >= 0x0600 && c <= 0x06E0)
                return true;
            i += Character.charCount(c);
        }
        return false;
    }

    @Override
    public NotificationCreatorUseCaseResponse process(NotificationCreatorUseCaseRequest request) {
        List<Notification> notifications = new ArrayList<>();
        for (NotificationMetaData metaData : request.getNotificationsMetaData()) {
            Notification notification = buildNotification(request, metaData);
            SMSNotificationContext smsContext = buildSmsContext(notification);
            notifications.add(notification);
            request.getModel().getNotificationModel().persist(notification);
            try {
                MPayActiveMq.sendMessage(QUEUE_NAME, smsContext.toString());
            } catch (Exception e) {
                notification.setSuccess(false);
            }
        }
        NotificationCreatorUseCaseResponse response = new NotificationCreatorUseCaseResponse();
        response.setSuccess(true);
        response.getNotifications().addAll(notifications);
        return response;
    }

    private SMSNotificationContext buildSmsContext(Notification notification) {
        SMSNotificationContext context = new SMSNotificationContext();
        context.setMessage(notification.getContent());
        context.setMobileNumber(notification.getReceiver());
        return context;
    }

    private Notification buildNotification(NotificationCreatorUseCaseRequest request, NotificationMetaData metaData) {
        Notification notification = new Notification();
        notification.setChannelId(metaData.getChannelTypeId());
        notification.setContent(evaluateVelocityTemplate(metaData.getData().get("context"), metaData.getTemplate()));
        notification.setReceiver(metaData.getReceiver());
        notification.setSuccess(true);
        notification.setReferenceMessageId(request.getReferenceMessageId());
        notification.setTenantId(request.getTenant());
        notification.setOrgId(request.getOrgId());
        return notification;
    }

//    private void integrateWithBank(Notification notification, IModelFactory model) {
//        SMSNotificationContext context = new SMSNotificationContext();
//        context.setMessage(notification.getContent());
//        context.setMobileNumber(notification.getReceiver());
//
//        SMSNotificationResponse response = SMSNotificationService.integrate(context, model);
//        if (response != null && response.isSuccess() != null && response.isSuccess())
//            model.getLogModel().debug("SUCCESS");
//        else
//            model.getLogModel().debug("FAIL");
//    }

    private String evaluateVelocityTemplate(Object notificationContext, String template) {
        try {
            StringWriter body = new StringWriter();
            VelocityContext context = new VelocityContext();
            context.put("context", notificationContext);
            ve.evaluate(context, body, "PAYMSG", template);
            return body.toString();
        } catch (IOException e) {
            throw new GenericException("Failed to evaluate Velocity for template: " + template, e);
        }
    }
}
