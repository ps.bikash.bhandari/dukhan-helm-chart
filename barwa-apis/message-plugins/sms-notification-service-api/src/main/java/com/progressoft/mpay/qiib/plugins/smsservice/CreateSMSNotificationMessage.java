package com.progressoft.mpay.barwa.plugins.smsservice;

import com.progressoft.mpay.common.BankIntegrationProcessingStatuses;
import com.progressoft.mpay.common.SystemConfigurationKeys;
import com.progressoft.mpay.entities.BankIntegrationMessage;
import com.progressoft.mpay.entities.Currency;
import com.progressoft.mpay.barwa.plugins.smsservice.context.SMSNotificationContext;
import com.progressoft.mpay.barwa.plugins.smsservice.util.GenerateRand13Digits;
import com.progressoft.mpay.models.IModelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.Calendar;

import static com.progressoft.mpay.common.SystemConfigurationKeys.DEFAULT_CURRENCY;

public class CreateSMSNotificationMessage {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateSMSNotificationMessage.class);

    public static BankIntegrationMessage createBankIntegrationMessage(SMSNotificationContext context, IModelFactory factory) {
        String tenantName = factory.getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.TENANT_NAME.getValue()).getValue();
        String orgId = factory.getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.ORG_ID.getValue()).getValue();
        String defaultBank = factory.getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.DEFAULT_BANK_CODE.getValue()).getValue();
        String defaultCurrency = factory.getSystemConfigurationModel().getSystemConfig(DEFAULT_CURRENCY.getValue()).getValue();
        try {
            LOGGER.debug("CreateMessage==========");
            BankIntegrationMessage message = new BankIntegrationMessage();
            message.setRequestId(GenerateRand13Digits.unique13digits());
            message.setRequestDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
            message.setRequestContent(context.toString());
            message.setReversed(false);
            message.setReversal(false);
            message.setTenantId(tenantName);
            message.setOrgId(Long.parseLong(orgId));
            message.setBankId(factory.getBankModel().get(defaultBank, tenantName).getId());
            message.setMessageIntegType("0");
            message.setStatus(BankIntegrationProcessingStatuses.PENDING);
            message.setCurrencyId(factory.getLookupsModel().get(defaultCurrency, Currency.class, tenantName).getId());
            return message;
        } catch (Exception ex) {
            LOGGER.error("Error when CreateMessage in createBankIntegrationMessage", ex);
        }
        return null;
    }
}
