package com.progressoft.mpay.barwa.plugins.customerregistration;

import com.progressoft.mpay.common.Reasons;
import com.progressoft.mpay.entities.ValidationResult;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.ProcessingUseCaseRequest;

public class CustomerValidator implements IValidator {

	@Override
	public ValidationResult validate(ProcessingUseCaseRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		if ((long) request.getExtraData().get(Constants.IS_CUSTOMER_REGISTERED) > 0)
			return new ValidationResult(Reasons.CUSTOMER_ALREADY_REGISTERED, null, false);
		return ValidationResult.getValid();
	}
}
