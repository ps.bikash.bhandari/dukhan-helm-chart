package com.progressoft.mpay.barwa.plugins.customerregistration;

import com.progressoft.mpay.common.*;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.MessageProcessingRequest;
import com.progressoft.mpay.usecases.messages.MessageProcessingResponse;
import com.progressoft.mpay.usecases.messages.MessageProcessingUseCase;
import com.progressoft.mpay.usecases.messages.validators.LanguageValidator;
import com.progressoft.mpay.usecases.messages.validators.RequestTypeValidator;
import com.progressoft.mpay.usecases.mpclear.MPClearSideInfoBuilder;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CustomerRegistrationMessageProcessingUseCase extends MessageProcessingUseCase {

    private static final Long SMS_NOTIFICATION_CHANNEL_TYPE_ID = 1l;

    private static String getCustomerName(Customer customer) {
        String value = customer.getFirstName();
        if (customer.getMiddleName() != null)
            value = value + " " + customer.getMiddleName();
        value = value + " " + customer.getLastName();
        return value;
    }

    @Override
    protected boolean loadBySessionId() {
        return false;
    }

    @Override
    protected OriginatorTypes getSenderType() {
        return OriginatorTypes.MOBILE;
    }

    @Override
    protected OriginatorTypes getReceiverType() {
        return OriginatorTypes.NONE;
    }

    @Override
    protected void preProcessRequest(MessageProcessingRequest request) {
        request.setCoreRequest(this.getCoreRequest(request));
        request.setMessageType(request.getModelFactory().getLookupsModel().get(request.getOperation().getMessageTypeId(), MessageType.class));
        CustomerRegistrationRequest message = (CustomerRegistrationRequest) request.getCoreRequest();
        if (checkSenderCustomer(request, message)) return;
        if (checkSenderMobile(request, message)) return;
        if (checkAlias(request, message)) return;
        IDType idType = checkIdType(request, message);
        if (idType == null) return;
        request.getSender().setCustomer(request.getModelFactory().getCustomerModel().get(idType.getId(), message.getIdNumber(), request.getTenant()));
        if (request.getSender().getCustomer() != null)
            return;
        loadLookups(request, message);
    }

    protected CoreRequest getCoreRequest(MessageProcessingRequest request) {
        return CustomerRegistrationRequest.parseMessage(request.getCoreRequest());
    }

    @Override
    protected List<IValidator> getValidators(OriginatorTypes senderType) {
        if (senderType == null || !senderType.equals(OriginatorTypes.MOBILE))
            throw new NotSupportedException("senderType: " + Objects.requireNonNull(senderType).getValue());
        List<IValidator> validators = new ArrayList<>();
        validators.add(new LanguageValidator());
        validators.add(new RequestTypeValidator());
        validators.add(new CustomerValidator());
        validators.add(new MobileValidator());
        validators.add(new MetaDataValidator());
        return validators;
    }

    @Override
    protected List<NotificationMetaData> createNotifications(MessageProcessingRequest request, ProcessingStatuses processingStatusCode) {
        return new ArrayList<>();
    }

    @Override
    protected void postAcceptRequest(MessageProcessingRequest request, MessageProcessingResponse response) {
        CustomerRegistrationRequest message = (CustomerRegistrationRequest) request.getCoreRequest();
        boolean isStandAlone = isStandAlone(request.getModelFactory(), request.getTenant());

        request.getSender().setCustomer(createCustomer(request, message, isStandAlone));
        if (request.getSender().getCustomer().getStatusCode().equals(Constants.CUSTOMER_APPROVED_STATUS_CODE))
            request.getSender().setAccount(createAccount(request, request.getSender().getCustomer()));
        request.getSender().setMobile(createMobile(request, message, isStandAlone, request.getSender().getCustomer()));
        request.getSender().setMobileAccount(createMobileAccount(request, message, isStandAlone, request.getSender().getCustomer()));
        response.setMpclearIntegrationMetaData(createMPClearMetaData(request));
    }

    @Override
    public void postMPClearPersistance(MessageProcessingRequest request, MessageProcessingResponse response, MPClearIntegrationMessage mpclearMessage) {
        CustomerIntegrationMessage message = new CustomerIntegrationMessage();
        message.setActionTypeId(CustomerRegistrationActionTypes.ADD_CLIENT.getValue());
        message.setCustomerId(request.getSender().getCustomer().getId());
        message.setId(mpclearMessage.getId());
        message.setIntegrationMessageTypeId(request.getModelFactory().getLookupsModel().get("1700", MPClearIntegrationMessageType.class, request.getTenant()).getId());
        message.setMessageLogId(mpclearMessage.getId());
        message.setMobileId(request.getSender().getMobile().getId());
        message.setMpClearMessageId(mpclearMessage.getMessageId());
        message.setOrgId(mpclearMessage.getOrgId());
        message.setTenantId(mpclearMessage.getTenantId());
        request.getModelFactory().getCustomerIntegrationMessageModel().persist(message);
        request.getSender().getCustomer().setMpClearMessageId(mpclearMessage.getId());
        request.getModelFactory().getCustomerModel().update(request.getSender().getCustomer());
    }

    @Override
    protected void persistPluginData(MessageProcessingRequest request, MessageProcessingResponse response) {
        if (!response.getProcessingStatus().equals(ProcessingStatuses.ACCEPTED))
            return;

        request.getModelFactory().getCustomerModel().persist(request.getSender().getCustomer());
        request.getSender().getMobile().setCustomerId(request.getSender().getCustomer().getId());
        request.getModelFactory().getCustomerMobileModel().persist(request.getSender().getMobile());
        if (request.getSender().getAccount() != null) {
            request.getModelFactory().getAccountModel().persist(request.getSender().getAccount());
            request.getSender().getMobileAccount().setAccountId(request.getSender().getAccount().getId());
        }
        request.getSender().getMobileAccount().setMobileId(request.getSender().getMobile().getId());
        request.getModelFactory().getMobileAccountModel().persist(request.getSender().getMobileAccount());
    }

    private Customer createCustomer(MessageProcessingRequest request, CustomerRegistrationRequest message, boolean isStandAlone) {
        Customer customer = new Customer();
        customer.setActive(true);
        customer.setBulkRegistrationId(request.getModelFactory().getSystemConfigurationModel().getLong(SystemConfigurationKeys.BULK_REGISTRATION_DEFAULT_ID.getValue(), request.getTenant()));
        customer.setClientReference(message.getReference());
        customer.setClientTypeId(((ClientType) request.getExtraData().get(Constants.CLIENT_TYPE_KEY)).getId());
        customer.setDateOfBirth(message.getDateOfBirth());
        customer.setCityId(((City) request.getExtraData().get(Constants.CITY_KEY)).getId());
        customer.setFirstName(message.getFirstName());
        customer.setMiddleName(message.getMidName());
        customer.setLastName(message.getLastName());
        customer.setFullName(customer.getFirstName() + " " + customer.getMiddleName() + " " + customer.getLastName());
        customer.setIdNumber(message.getIdNumber());
        customer.setNationalityId(((Country) request.getExtraData().get(Constants.COUNTRY_KEY)).getId());
        customer.setIdTypeId(((IDType) request.getExtraData().get(Constants.CLIENT_ID_TYPE_KEY)).getId());
        customer.setIdNumber(message.getIdNumber());
        customer.setPreferedLanguageId(((Language) request.getExtraData().get(Constants.LANGUAGE_KEY)).getId());
        customer.setRegistered(true);
        customer.setState(EntityStates.NEW);
        customer.setTenantId(request.getTenant());
        customer.setOrgId(request.getOrgId());
        customer.setAddress(message.getAddress());
        customer.setIsBeneficiary("1");
        customer.setStatusCode(getCustomerStatus(isStandAlone));
        customer.setMobileNumber(message.getSender());
        if (message.getCustomerBankUnBank() != null && !message.getCustomerBankUnBank().isEmpty())
            customer.setBankedunbanked(message.getCustomerBankUnBank());
        else customer.setBankedunbanked(BankedUnbankedFlag.BANKED.getValue());
        customer.setAlias(message.getAlias());
        customer.setBankId(request.getSender().getBank().getId());
        customer.setProfileId(((Profile) request.getExtraData().get(Constants.PROFILE_KEY)).getId());
        customer.setRetryCount(0L);
        return customer;
    }

    private CustomerMobile createMobile(MessageProcessingRequest request, CustomerRegistrationRequest message, boolean isStandAlone, Customer customer) {
        CustomerMobile mobile = new CustomerMobile();
        mobile.setActive(true);
        mobile.setBankId(request.getSender().getBank().getId());
        mobile.setPin(message.getPin());
        mobile.setPinLastChanged(request.getModelFactory().getDateUtilsModel().getCurrentTimestamp());
        mobile.setCustomerId(request.getSender().getCustomer().getId());
        mobile.setMaxNumberOfDevices(0);
        mobile.setMobileNumber(request.getSender().getCustomer().getMobileNumber());
        mobile.setAlias(request.getSender().getCustomer().getAlias());
        mobile.setNotificationShowType(NotificationShowTypes.MOBILE_NUMBER);
        mobile.setRegistered(isStandAlone);
        mobile.setState(EntityStates.NEW);
        mobile.setTenantId(request.getTenant());
        mobile.setOrgId(request.getOrgId());
        mobile.setMaxNumberOfDevices(0);
        mobile.setEnableEmail(true);
        mobile.setBankedunbanked(customer.getBankedunbanked());
        mobile.setBanked(BankedUnbankedFlag.BANKED.getValue().equals(customer.getBankedunbanked()));
        mobile.setEnablePushNotification(true);
        mobile.setEnableSMS(true);
        mobile.setStatusCode(getMobileStatus(isStandAlone));
        mobile.setProfileId(request.getSender().getCustomer().getProfileId());
        mobile.setOperator(request.getSender().getCustomer().getOperator());
        mobile.setChannelTypeId(SMS_NOTIFICATION_CHANNEL_TYPE_ID);
        mobile.setBlocked(false);
        return mobile;
    }

    private MobileAccount createMobileAccount(MessageProcessingRequest request, CustomerRegistrationRequest message, boolean isStandAlone, Customer customer) {
        MobileAccount mobileAccount = new MobileAccount();
        if (request.getSender().getAccount() != null)
            mobileAccount.setAccountId(request.getSender().getMobile().getId());
        mobileAccount.setAccountSelector(1L);
        mobileAccount.setActive(true);
        mobileAccount.setBanked(BankedUnbankedFlag.BANKED.getValue().equals(customer.getBankedunbanked()));
        mobileAccount.setBankId(request.getSender().getMobile().getBankId());
        mobileAccount.setDefault(true);
        mobileAccount.setMobileId(request.getSender().getMobile().getId());
        mobileAccount.setProfileId(request.getSender().getMobile().getProfileId());
        mobileAccount.setRegistered(isStandAlone);
        mobileAccount.setState(EntityStates.NEW);
        mobileAccount.setSwitchDefault(true);
        mobileAccount.setTenantId(request.getSender().getMobile().getTenantId());
        mobileAccount.setOrgId(request.getSender().getMobile().getOrgId());
        mobileAccount.setStatusCode(getMobileAccountStatus(isStandAlone));
        return mobileAccount;
    }

    private Account createAccount(MessageProcessingRequest request, Customer customer) {
        Currency currency = request.getModelFactory().getLookupsModel().get(request.getModelFactory().getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.DEFAULT_CURRENCY.getValue(), request.getTenant()).getValue(), Currency.class, request.getTenant());
        Account account = new Account();
        account.setAccountName(request.getCoreRequest().getSender());
        account.setAccountNumber(String.valueOf(request.getModelFactory().getAccountModel().getNextMobileAccount()));
        account.setActive(true);
        account.setBanked(BankedUnbankedFlag.BANKED.getValue().equals(customer.getBankedunbanked()));
        account.setBalance(BigDecimal.ZERO);
        account.setBalanceType(BalanceTypes.DEBIT);
        account.setCurrencyId(currency.getId());
        account.setTenantId(request.getTenant());
        account.setOrgId(request.getOrgId());
        account.setType(AccountTypes.WALLET);
        account.setNature(AccountNatures.ASSETS);
        account.setMinBalance(BigDecimal.ZERO);
        return account;
    }

    private MPClearIntegrationMetaData createMPClearMetaData(MessageProcessingRequest request) {
        if (!request.getSender().getCustomer().getStatusCode().equals(Constants.CUSTOMER_INTEGRATION_STATUS_CODE))
            return null;
        MPClearIntegrationMetaData metaData = new MPClearIntegrationMetaData();
        metaData.setType(MPClearIntegrationTypes.REGISTRATION_REQUEST);
        metaData.getExtraData().put(MPClearMetaDataKeys.IS_ENCODED, true);
        metaData.getExtraData().put(MPClearMetaDataKeys.ID, request.getMessage().getId());
        metaData.getExtraData().put(MPClearMetaDataKeys.DATE, request.getModelFactory().getDateUtilsModel().getCurrentTimestamp());
        metaData.getExtraData().put(MPClearMetaDataKeys.REGISTRATION_MODE, Constants.ADD_CLIENT_ACTION);
        metaData.getExtraData().put(MPClearMetaDataKeys.CURRENCY, request.getModelFactory().getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.DEFAULT_CURRENCY.getValue(), request.getTenant()).getValue());
        metaData.getExtraData().put(MPClearMetaDataKeys.SENDER_INFO, request.getSender().getMobile().getMobileNumber());
        metaData.getExtraData().put(MPClearMetaDataKeys.SENDER_TYPE, OriginatorTypes.MOBILE);
        metaData.getExtraData().put(MPClearMetaDataKeys.SENDER_ACCOUNT, MPClearSideInfoBuilder.getAccount(request.getModelFactory(), request.getSender().getBank().getCode(), request.getSender().getMobileAccount().getAccountSelector(), request.getTenant()));
        metaData.getExtraData().put(MPClearMetaDataKeys.REGISTRATION_CITY_CODE, ((City) request.getExtraData().get(Constants.CITY_KEY)).getDescription());
        metaData.getExtraData().put(MPClearMetaDataKeys.REGISTRATION_NATIONALITY_CODE, ((Country) request.getExtraData().get(Constants.COUNTRY_KEY)).getCountryIsocode());
        metaData.getExtraData().put(MPClearMetaDataKeys.REGISTRATION_EXISTANCE_DATE, request.getModelFactory().getDateUtilsModel().format(new Timestamp(request.getSender().getCustomer().getDateOfBirth().getTime()), Constants.ISO_DATE_FORMAT));
        metaData.getExtraData().put(MPClearMetaDataKeys.REGISTRATION_CLIENT_TYPE, ((ClientType) request.getExtraData().get(Constants.CLIENT_TYPE_KEY)).getCode());
        metaData.getExtraData().put(MPClearMetaDataKeys.REGISTRATION_CLIENT_IDENTIFIER, request.getSender().getCustomer().getIdNumber());
        metaData.getExtraData().put(MPClearMetaDataKeys.REGISTRATION_CLIENT_ID_TYPE, ((IDType) request.getExtraData().get(Constants.CLIENT_ID_TYPE_KEY)).getCode());
        metaData.getExtraData().put(MPClearMetaDataKeys.ADDRESS, buildAddress(request.getSender().getCustomer()));
        metaData.getExtraData().put(MPClearMetaDataKeys.CORRELATION_ID, getNextMPClearMessageId(request.getModelFactory(), request.getModelFactory().getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.DEFAULT_BANK_CODE.getValue(), request.getTenant()).getValue()));
        metaData.getExtraData().put(MPClearMetaDataKeys.REGISTRATION_CLIENT_NAME, getCustomerName(request.getSender().getCustomer()));
        metaData.getExtraData().put(MPClearMetaDataKeys.REGISTRATION_REFERENCE, request.getSender().getCustomer().getClientReference());
        metaData.getExtraData().put(MPClearMetaDataKeys.EXTRA_DATA, request.getSender().getCustomer().getNote());
        String gender = request.getSender().getCustomer().getGender();
        if (gender != null && !gender.isEmpty())
            metaData.getExtraData().put(MPClearMetaDataKeys.REGISTRATION_GENDER, gender.equals("1") ? "MALE" : "FEMALE");
        metaData.getExtraData().put(MPClearMetaDataKeys.REFERENCE, request.getMessage().getReference());
        metaData.getExtraData().put(MPClearMetaDataKeys.HINT, request.getSender().getMobile().getId() + ";M");
        metaData.getExtraData().put(MPClearMetaDataKeys.ISDEFAULTACCOUNT, request.getSender().getMobileAccount().isDefault());
        metaData.getExtraData().put(MPClearMetaDataKeys.ISBANKEDACCOUNT, request.getSender().getMobileAccount().isBanked());
        return metaData;
    }

    private String buildAddress(Customer customer) {
        StringBuilder builder = new StringBuilder();

        if (!isBlank(customer.getBuildingNumber()))
            builder.append("BuildingNumber:").append(customer.getBuildingNumber()).append("|");
        if (!isBlank(customer.getPhoneOne()))
            builder.append("Phone1:").append(customer.getPhoneOne()).append("|");
        if (!isBlank(customer.getPhoneTwo()))
            builder.append("Phone2:").append(customer.getPhoneTwo()).append("|");
        if (!isBlank(customer.getPoBox()))
            builder.append("POBox:").append(customer.getPoBox()).append("|");
        if (!isBlank(customer.getZipCode()))
            builder.append("ZipCode:").append(customer.getZipCode()).append("|");
        if (!isBlank(customer.getStreetName()))
            builder.append("StreetName:").append(customer.getStreetName()).append("|");
        if (!isBlank(customer.getEmail()))
            builder.append("Email:").append(customer.getEmail());

        if (builder.length() > 0 && builder.charAt(builder.length() - 1) == '|')
            return builder.substring(0, builder.length() - 1);
        else
            return builder.toString();
    }

    private boolean isBlank(String value) {
        return value == null || value.trim().length() == 0;
    }

    private void loadLookups(MessageProcessingRequest request,CustomerRegistrationRequest message) {
        Bank bank = request.getModelFactory().getBankModel().get(request.getModelFactory().getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.CUSTOMER_REGISTRATION_BANK_CODE.getValue(), request.getTenant()).getValue(), request.getTenant());
        if (bank == null) return;
        request.getSender().setBank(bank);
        String personClientTypeCode = request.getModelFactory().getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.PERSON_CLIENT_TYPE.getValue(), request.getTenant()).getValue();
        ClientType clientType = request.getModelFactory().getLookupsModel().get(personClientTypeCode, ClientType.class, request.getTenant());
        if (clientType == null) return;
        request.getExtraData().put(Constants.CLIENT_TYPE_KEY, clientType);

        Profile profile = request.getModelFactory().getProfileModel().get(message.getProfileCode(), request.getTenant());
        if (profile == null) return;
        request.getExtraData().put(Constants.PROFILE_KEY, profile);
        Country country = request.getModelFactory().getLookupsModel().get(message.getCountryCode(), Country.class, request.getTenant());
        if (country == null) return;
        request.getExtraData().put(Constants.COUNTRY_KEY, country);
        City city = request.getModelFactory().getLookupsModel().get(message.getCityCode(), City.class, request.getTenant());
        if (city == null) return;
        request.getExtraData().put(Constants.CITY_KEY, city);
        Language language = request.getModelFactory().getLanguageModel().get(message.getPrefLanguage());
        if (language == null) return;
        request.getExtraData().put(Constants.LANGUAGE_KEY, language);
    }

    private IDType checkIdType(MessageProcessingRequest request, CustomerRegistrationRequest message) {
        IDType idType = request.getModelFactory().getLookupsModel().get(message.getIdTypeCode(), IDType.class, request.getTenant());
        if (idType == null)
            return null;
        request.getExtraData().put(Constants.CLIENT_ID_TYPE_KEY, idType);
        return idType;
    }

    private boolean checkSenderCustomer(MessageProcessingRequest request, CustomerRegistrationRequest message) {
        IDType idType = request.getModelFactory().getLookupsModel().get(message.getIdTypeCode(), IDType.class, request.getTenant());
        long present = request.getModelFactory().getCustomerModel().isCustomerPresent(idType.getId(), message.getIdNumber(), true);
        request.getExtraData().put(Constants.IS_CUSTOMER_REGISTERED, present);
        return present > 0;
    }

    private boolean checkSenderMobile(MessageProcessingRequest request, CustomerRegistrationRequest message) {
        long present = request.getModelFactory().getCustomerMobileModel().isPresent(message.getSender(), OriginatorTypes.MOBILE, false);
        request.getExtraData().put(Constants.IS_MOBILE_REGISTERED, present);
        return present > 0;
    }

    private boolean checkAlias(MessageProcessingRequest request, CustomerRegistrationRequest message) {
        request.getExtraData().put(Constants.MOBILE_BY_ALIAS_KEY, 0L);
        if (message.getAlias() != null && message.getAlias().trim().length() > 0) {
            long present = request.getModelFactory().getCustomerMobileModel().isPresent(message.getAlias(), OriginatorTypes.ALIAS, false);
            request.getExtraData().put(Constants.MOBILE_BY_ALIAS_KEY, present);
            return present > 0;
        }
        return false;
    }

    private String getCustomerStatus(boolean isStandAlone) {
        if (isStandAlone)
            return Constants.CUSTOMER_APPROVED_STATUS_CODE;
        else
            return Constants.CUSTOMER_INTEGRATION_STATUS_CODE;
    }

    private String getMobileStatus(boolean isStandAlone) {
        if (isStandAlone)
            return Constants.CUSTOMER_MOBILE_APPROVED_STATUS_CODE;
        else return Constants.CUSTOMER_MOBILE_INTEGRATION_STATUS_CODE;
    }

    private String getMobileAccountStatus(boolean isStandAlone) {
        if (isStandAlone)
            return Constants.MOBILE_ACCOUNT_APPROVED_STATUS_CODE;
        else
            return Constants.MOBILE_ACCOUNT_INTEGRATION_STATUS_CODE;
    }
}