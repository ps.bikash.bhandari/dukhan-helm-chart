package com.progressoft.mpay.barwa.plugins.customerregistration;

import com.progressoft.mpay.common.MPClearIntegrationTypes;
import com.progressoft.mpay.exceptions.NotImplementedException;
import com.progressoft.mpay.usecases.messages.IMessageProcessingActionsUseCase;
import com.progressoft.mpay.usecases.messages.IMessageProcessingUseCaseFactory;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingUseCase;
import com.progressoft.mpay.usecases.messages.MessageProcessingUseCase;

public class CustomerRegistrationMessageProcessingUseCaseFactory implements IMessageProcessingUseCaseFactory {

	@Override
	public MessageProcessingUseCase getMessageProcessingUseCase() {
		return new CustomerRegistrationMessageProcessingUseCase();
	}

	@Override
	public IntegrationProcessingUseCase getIntegrationProcessingUseCase(MPClearIntegrationTypes type) {
		throw new NotImplementedException("getIntegrationProcessingUseCase");
	}

	@Override
	public IMessageProcessingActionsUseCase getMessageProcessingActionsUseCase() {
		throw new NotImplementedException("getMessageProcessingActionsUseCase");
	}
	
}