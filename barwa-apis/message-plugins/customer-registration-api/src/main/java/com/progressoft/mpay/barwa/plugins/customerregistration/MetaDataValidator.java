package com.progressoft.mpay.barwa.plugins.customerregistration;

import com.progressoft.mpay.common.Reasons;
import com.progressoft.mpay.entities.ValidationResult;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.ProcessingUseCaseRequest;

public class MetaDataValidator implements IValidator {
    @Override
    public ValidationResult validate(ProcessingUseCaseRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        if (request.getSender().getBank() == null)
            return new ValidationResult(Reasons.BANK_NOT_DEFINED, null, false);
        if (!request.getExtraData().containsKey(Constants.CLIENT_ID_TYPE_KEY))
            return new ValidationResult(Reasons.INVALID_ID_TYPE, null, false);
        if (!request.getExtraData().containsKey(Constants.CLIENT_TYPE_KEY))
            return new ValidationResult(Reasons.INVALID_CLIENT_ID_NUMBER, null, false);
        if (!request.getExtraData().containsKey(Constants.PROFILE_KEY))
            return new ValidationResult(Reasons.PROFILE_NOT_DEFINED, null, false);
        if (!request.getExtraData().containsKey(Constants.COUNTRY_KEY))
            return new ValidationResult(Reasons.INVALID_NATIONALITY, null, false);
        if (!request.getExtraData().containsKey(Constants.CITY_KEY))
            return new ValidationResult(Reasons.CITY_NOT_DEFINED, null, false);
        if (!request.getExtraData().containsKey(Constants.LANGUAGE_KEY))
            return new ValidationResult(Reasons.LANGUAGE_NOT_DEFINED, null, false);
        return ValidationResult.getValid();
    }

}
