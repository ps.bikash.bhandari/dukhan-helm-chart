package com.progressoft.mpay.barwa.plugins.customerregistration;

import com.progressoft.mpay.common.Reasons;
import com.progressoft.mpay.common.SystemConfigurationKeys;
import com.progressoft.mpay.entities.ValidationResult;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.MessageProcessingRequest;
import com.progressoft.mpay.usecases.messages.ProcessingUseCaseRequest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MobileValidator implements IValidator {

	@Override
	public ValidationResult validate(ProcessingUseCaseRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		if ((long) request.getExtraData().get(Constants.IS_MOBILE_REGISTERED) > 0)
			return new ValidationResult(Reasons.MOBILE_ALREADY_REGISTERED, null, false);
		if (!isValidMobileNumber(request))
			return new ValidationResult(Reasons.INVALID_MOBILE_NUMBER, null, false);
		if ((long) request.getExtraData().get(Constants.MOBILE_BY_ALIAS_KEY) > 0)
			return new ValidationResult(Reasons.ALIAS_IS_DEFINED, null, false);
		return ValidationResult.getValid();
	}

	private boolean isValidMobileNumber(ProcessingUseCaseRequest request) {
		String mobilePattern = request.getModelFactory().getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.MOBILE_NUMBER_REGEX.getValue(), request.getTenant()).getValue();
		Pattern pattern = Pattern.compile(mobilePattern);
		Matcher matcher = pattern.matcher((((MessageProcessingRequest)request).getCoreRequest()).getSender());
		return matcher.matches();
	}
}
