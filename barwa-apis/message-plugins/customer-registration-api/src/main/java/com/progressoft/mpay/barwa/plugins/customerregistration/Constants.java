package com.progressoft.mpay.barwa.plugins.customerregistration;

public class Constants {
	
	public static final String CLIENT_ID_TYPE_KEY = "IdTypeKey";
	public static final String BENEFICARY_ID_TYPE_KEY = "benefIdType";
	public static final String CLIENT_TYPE_KEY = "ClientTypeKey";
	public static final String MOBILE_BY_ALIAS_KEY = "mobileByAlias";
	public static final String ID_TYPE_KEY = "IdTypeKey";
	public static final String COUNTRY_KEY = "CountryKey";
	public static final String CITY_KEY = "CityKey";
	public static final String LANGUAGE_KEY = "LanguageKey";
	public static final String PROFILE_KEY = "ProfileKey";

	public static final String ADD_CLIENT_ACTION = "ADCL";
	public static final String ISO_DATE_FORMAT = "yyyyMMdd";
	public static final String ISO_TIME_FORMAT = "MMddHHmmss";
	
	public static final String CUSTOMER_PENDING_APPROVAL_STATUS_CODE = "100002";
	public static final String CUSTOMER_APPROVED_STATUS_CODE = "100005";
	public static final String CUSTOMER_INTEGRATION_STATUS_CODE = "100007";
	
	public static final String CUSTOMER_MOBILE_PENDING_APPROVAL_STATUS_CODE = "100102";
	public static final String CUSTOMER_MOBILE_APPROVED_STATUS_CODE = "100105";
	public static final String CUSTOMER_MOBILE_INTEGRATION_STATUS_CODE = "100107";
	
	public static final String MOBILE_ACCOUNT_PENDING_APPROVAL_STATUS_CODE = "101002";
	public static final String MOBILE_ACCOUNT_APPROVED_STATUS_CODE = "101005";
	public static final String MOBILE_ACCOUNT_INTEGRATION_STATUS_CODE = "101007";

	public static final String IS_MOBILE_REGISTERED = "isMobileRegistered";
	public static final String IS_CUSTOMER_REGISTERED = "isCustomerRegistered";
}
