package com.progressoft.mpay.barwa.plugins.customerregistration;

import com.progressoft.mpay.common.OriginatorTypes;
import com.progressoft.mpay.entities.CoreRequest;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NullArgumentException;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomerRegistrationRequest extends CoreRequest {
    public static final String BIRTH_DATE_FORMAT = "dd/MM/yyyy";
    private static final String FIRST_NAME_KEY = "firstName";
    private static final String MID_NAME_KEY = "midName";
    private static final String LAST_NAME_KEY = "lastName";
    private static final String ID_TYPE_CODE_KEY = "idTypeCode";
    private static final String ID_NUMBER_KEY = "idNumber";
    private static final String DATE_OF_BIRTH_KEY = "dateOfBirth";
    private static final String REFERENCE_KEY = "reference";
    private static final String COUNTRY_CODE_KEY = "countryCode";
    private static final String CITY_CODE_KEY = "cityCode";
    private static final String PREF_LANGUAGE_KEY = "prefLanguage";
    private static final String ALIAS_KEY = "alias";
    private static final String ADDRESS_KEY = "address";
    private static final String CUSTOMER_BANK_UN_BANK = "customerBankUnBank";
    private static final String CLIENT_TYPE = "clientType";
    private static final String PROFILE_CODE = "profileCode";

    private String firstName;
    private String midName;
    private String lastName;
    private String idTypeCode;
    private String idNumber;
    private Date dateOfBirth;
    private String reference;
    private String countryCode;
    private String cityCode;
    private long prefLanguage;
    private String alias;
    private String address;
    private String customerBankUnBank;
    private String clientType;
    private String profileCode;

    public CustomerRegistrationRequest(CoreRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setExtraData(request.getExtraData());
    }

    public static CustomerRegistrationRequest parseMessage(CoreRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        CustomerRegistrationRequest message = new CustomerRegistrationRequest(request);
        validateGenericFields(message);
        validateCustomerFields(message);
        message.setReference(message.getValue(REFERENCE_KEY));
        if (message.getReference() == null || message.getReference().trim().length() == 0)
            throw new MessageParsingException(REFERENCE_KEY);
        message.setCountryCode(message.getValue(COUNTRY_CODE_KEY));
        if (message.getCountryCode() == null || message.getCountryCode().trim().length() == 0)
            throw new MessageParsingException(COUNTRY_CODE_KEY);
        message.setCityCode(message.getValue(CITY_CODE_KEY));
        if (message.getCityCode() == null || message.getCityCode().trim().length() == 0)
            throw new MessageParsingException(CITY_CODE_KEY);
        try {
            message.setPrefLanguage(Long.parseLong(message.getValue(PREF_LANGUAGE_KEY)));
        } catch (Exception e) {
            throw new MessageParsingException(PREF_LANGUAGE_KEY);
        }
        message.setAlias(message.getValue(ALIAS_KEY));
        message.setAddress(message.getValue(ADDRESS_KEY));
        if (message.getAddress() == null || message.getAddress().trim().length() == 0)
            throw new MessageParsingException(ADDRESS_KEY);
        message.setCustomerBankUnBank(message.getValue(CUSTOMER_BANK_UN_BANK));
        if (message.getCustomerBankUnBank() != null && !message.getCustomerBankUnBank().isEmpty()) {
            if (!message.getCustomerBankUnBank().equalsIgnoreCase("1") && !message.getCustomerBankUnBank().equalsIgnoreCase("2"))
                throw new MessageParsingException(CUSTOMER_BANK_UN_BANK);
        }
        message.setClientType(message.getValue(CLIENT_TYPE));
        return message;
    }

    private static void validateCustomerFields(CustomerRegistrationRequest message) throws MessageParsingException {
        message.setFirstName(message.getValue(FIRST_NAME_KEY));
        if (CoreRequest.isBlank(message.getFirstName()))
            throw new MessageParsingException(FIRST_NAME_KEY);
        message.setMidName(message.getValue(MID_NAME_KEY));
        if (CoreRequest.isBlank(message.getMidName()))
            throw new MessageParsingException(MID_NAME_KEY);
        message.setLastName(message.getValue(LAST_NAME_KEY));
        if (CoreRequest.isBlank(message.getLastName()))
            throw new MessageParsingException(LAST_NAME_KEY);
        message.setIdTypeCode(message.getValue(ID_TYPE_CODE_KEY));
        if (CoreRequest.isBlank(message.getIdTypeCode()))
            throw new MessageParsingException(ID_TYPE_CODE_KEY);
        message.setIdNumber(message.getValue(ID_NUMBER_KEY));
        if (CoreRequest.isBlank(message.getIdNumber()))
            throw new MessageParsingException(ID_NUMBER_KEY);
        message.setProfileCode(message.getValue(PROFILE_CODE));
        if (CoreRequest.isBlank(message.getProfileCode()))
            throw new MessageParsingException(PROFILE_CODE);
        try {
            SimpleDateFormat format = new SimpleDateFormat(BIRTH_DATE_FORMAT);
            message.setDateOfBirth(format.parse(message.getValue(DATE_OF_BIRTH_KEY)));
        } catch (Exception e) {
            throw new MessageParsingException(DATE_OF_BIRTH_KEY);
        }
    }

    private static void validateGenericFields(CustomerRegistrationRequest message) throws MessageParsingException {
        if (message.getSender() == null || message.getSender().trim().length() == 0)
            throw new MessageParsingException(CoreRequest.SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(OriginatorTypes.MOBILE.getValue()))
            throw new MessageParsingException(CoreRequest.SENDER_TYPE_KEY);
    }

    public String getProfileCode() {
        return profileCode;
    }

    public void setProfileCode(String profileCode) {
        this.profileCode = profileCode;
    }

    public String getCustomerBankUnBank() {
        return customerBankUnBank;
    }

    public void setCustomerBankUnBank(String customerBankUnBank) {
        this.customerBankUnBank = customerBankUnBank;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMidName() {
        return midName;
    }

    public void setMidName(String midName) {
        this.midName = midName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdTypeCode() {
        return idTypeCode;
    }

    public void setIdTypeCode(String idTypeCode) {
        this.idTypeCode = idTypeCode;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public long getPrefLanguage() {
        return prefLanguage;
    }

    public void setPrefLanguage(long prefLanguage) {
        this.prefLanguage = prefLanguage;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
