package com.progressoft.mpay.barwa.plugins.customercashin;

import com.progressoft.mpay.common.MPClearIntegrationTypes;
import com.progressoft.mpay.common.ProcessingStatuses;
import com.progressoft.mpay.common.Reasons;
import com.progressoft.mpay.entities.NotificationMetaData;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingRequest;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingResponse;
import com.progressoft.mpay.usecases.messages.integrations.FinancialIntegrationProcessingUseCase;
import com.progressoft.mpay.usecases.messages.integrations.IntegrationProcessor;

import java.util.ArrayList;
import java.util.List;

public class CashInFinancialIntegrationProcessingUseCase extends FinancialIntegrationProcessingUseCase {
    public CashInFinancialIntegrationProcessingUseCase(MPClearIntegrationTypes type) {
        super(type);
    }

    public CashInFinancialIntegrationProcessingUseCase(IntegrationProcessor integrationProcessor) {
        super(integrationProcessor);
    }

    @Override
    protected List<NotificationMetaData> createNotifications(IntegrationProcessingRequest request, ProcessingStatuses processingStatusCode) {
        return new ArrayList<>();
    }

    @Override
    protected IntegrationProcessingResponse rejectRequest(IntegrationProcessingRequest request, Reasons reasonCode, String reasonDescription, ProcessingStatuses processingStatus) {
        preRejectRequest(request, reasonCode, reasonDescription, processingStatus);
        if (request.getOriginalMPclearMessage() != null)
            request.getOriginalMPclearMessage().setResponseReceived(true);
        IntegrationProcessingResponse response = IntegrationProcessingResponse.build(request.getResponseMetaData(), request, ProcessingStatuses.ACCEPTED, reasonCode, reasonDescription);
        postRejectRequest(request, response);
        if (request.getMessage() != null)
            response.getCoreMessage().setResponseContent(request.getModelFactory().getCoreResponseModel().getString(generateResponse(request, reasonCode, reasonDescription, processingStatus)));
        response.getNotificationsMetaData().addAll(createNotifications(request, processingStatus));
        return response;
    }
}
