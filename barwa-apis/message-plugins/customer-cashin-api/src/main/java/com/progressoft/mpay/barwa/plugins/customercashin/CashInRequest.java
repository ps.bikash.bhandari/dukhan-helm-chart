package com.progressoft.mpay.barwa.plugins.customercashin;

import com.progressoft.mpay.common.OriginatorTypes;
import com.progressoft.mpay.entities.CoreRequest;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NullArgumentException;

public class CashInRequest extends CoreRequest {
    private static final String BANK_ACCOUNT = "bankAccount";
    private static final String SENDER_NAME = "senderName";
    private static final String REF_NUM = "refNum";

    private String bankAccount;
    private String senderName;
    private String refNum;

    public CashInRequest(CoreRequest request) {
        if (request == null)
            throw new NullArgumentException("request");

        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setAmount(request.getAmount());
        setReceiver(request.getReceiver());
        setReceiverType(request.getReceiverType());
        setSenderAccount(request.getSenderAccount());
        setReceiverAccount(request.getReceiverAccount());
        setExtraData(request.getExtraData());
        setTenant(request.getTenant());
        setChecksum(request.getChecksum());
        setSessionId(request.getSessionId());
        setNotes(request.getNotes());
    }

    public static CashInRequest parseMessage(CoreRequest request) {
        if (request == null)
            return null;

        CashInRequest message = new CashInRequest(request);
        if (message.getSender() == null || message.getSender().length() == 0)
            throw new MessageParsingException(SENDER_KEY);

        if (message.getSenderType() == null || !message.getSenderType().trim().equals(OriginatorTypes.CORPORATE.getValue()))
            throw new MessageParsingException(SENDER_TYPE_KEY);

        message.setBankAccount(message.getValue(BANK_ACCOUNT));
        message.setSenderName(message.getValue(SENDER_NAME));
        message.setRefNum(message.getValue(REF_NUM));
        return message;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }


    public String getRefNum() {
        return refNum;
    }

    public void setRefNum(String refNum) {
        this.refNum = refNum;
    }


}
