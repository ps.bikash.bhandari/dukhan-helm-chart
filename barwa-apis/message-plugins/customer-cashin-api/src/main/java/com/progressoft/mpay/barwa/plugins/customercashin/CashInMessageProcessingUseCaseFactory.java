package com.progressoft.mpay.barwa.plugins.customercashin;

import com.progressoft.mpay.common.MPClearIntegrationTypes;
import com.progressoft.mpay.exceptions.NotImplementedException;
import com.progressoft.mpay.usecases.messages.FinancialMessageProcessingUseCaseFactory;
import com.progressoft.mpay.usecases.messages.IMessageProcessingActionsUseCase;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingUseCase;
import com.progressoft.mpay.usecases.messages.MessageProcessingUseCase;

public class CashInMessageProcessingUseCaseFactory extends FinancialMessageProcessingUseCaseFactory {

    @Override
    public MessageProcessingUseCase getMessageProcessingUseCase() {
        return new CashInMessageProcessingUseCase();
    }

    @Override
    public IntegrationProcessingUseCase getIntegrationProcessingUseCase(MPClearIntegrationTypes type) {
        return new CashInFinancialIntegrationProcessingUseCase(new CashInResponseProcessor());
    }

    @Override
    public IMessageProcessingActionsUseCase getMessageProcessingActionsUseCase() {
        throw new NotImplementedException("getMessageProcessingActionsUseCase");
    }


}