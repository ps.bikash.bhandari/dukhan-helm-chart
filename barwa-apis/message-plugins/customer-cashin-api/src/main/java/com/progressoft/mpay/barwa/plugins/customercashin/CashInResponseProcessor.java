package com.progressoft.mpay.barwa.plugins.customercashin;

import com.progressoft.mpay.usecases.messages.IntegrationProcessingRequest;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingResponse;
import com.progressoft.mpay.usecases.messages.integrations.ResponseProcessor;

public class CashInResponseProcessor extends ResponseProcessor {

    @Override
    protected void createMPClearResponse(IntegrationProcessingRequest request, IntegrationProcessingResponse response) {
    }
}