package com.progressoft.mpay.barwa.plugins.transactionreversal;

import com.progressoft.mpay.common.NotificationTypes;
import com.progressoft.mpay.common.OriginatorTypes;
import com.progressoft.mpay.common.ProcessingStatuses;
import com.progressoft.mpay.entities.*;
import com.progressoft.mpay.models.impl.psp.MessageTypeCodes;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.MessageProcessingRequest;
import com.progressoft.mpay.usecases.messages.MessageProcessingResponse;
import com.progressoft.mpay.usecases.messages.MessageProcessingUseCase;
import com.progressoft.mpay.usecases.messages.ProcessingRequestSide;
import com.progressoft.mpay.usecases.messages.notifications.NotificationContext;
import com.progressoft.mpay.usecases.messages.notifications.NotificationMetaDataBuilder;
import com.progressoft.mpay.usecases.messages.validators.LanguageValidator;
import com.progressoft.mpay.usecases.messages.validators.SenderTypeValidator;

import java.util.ArrayList;
import java.util.List;

public class TransactionReversalMessageProcessingUseCase extends MessageProcessingUseCase {

    public static final String TRANSACTION_BY_REF = "transactionByRef";
    public static final String MESSAGE_TYPE = "messageType";

    @Override
    protected boolean loadBySessionId() {
        return false;
    }

    @Override
    protected OriginatorTypes getSenderType() {
        return OriginatorTypes.CORPORATE;
    }

    @Override
    protected OriginatorTypes getReceiverType() {
        return OriginatorTypes.NONE;
    }

    @Override
    protected CoreRequest getCoreRequest(MessageProcessingRequest request) {
        return TransactionReversalRequest.parseMessage(request.getCoreRequest());
    }

    @Override
    protected void preProcessRequest(MessageProcessingRequest request) {
        super.preProcessRequest(request);
        TransactionReversalRequest reversalRequest = (TransactionReversalRequest) request.getCoreRequest();
        request.getExtraData().put(TRANSACTION_BY_REF, request.getModelFactory().getTransactionModel().getByTransactionReference(reversalRequest.getTransactionReferenceId(), request.getTenant()));
    }

    @Override
    protected List<IValidator> getValidators(OriginatorTypes senderType) {
        List<IValidator> validators = new ArrayList<>();
        validators.add(new SenderTypeValidator(senderType));
        validators.add(new LanguageValidator());
        validators.add(new CashInCashOutTransactionReversalValidator());
        return validators;
    }

    @Override
    protected List<NotificationMetaData> createNotifications(MessageProcessingRequest request, ProcessingStatuses processingStatusCode) {
        if (processingStatusCode.equals(ProcessingStatuses.ACCEPTED)) {
            Transaction transaction = (Transaction) request.getExtraData().get(TRANSACTION_BY_REF);
            MessageType messageType = (MessageType) request.getExtraData().get(MESSAGE_TYPE);
            CustomerMobile customerMobile = request.getModelFactory().getCustomerMobileModel().get(setMobileId(transaction, messageType));
            Customer customer = request.getModelFactory().getCustomerModel().get(customerMobile.getCustomerId());
            fillBeneficiarySide(request, customerMobile, customer);
            return new NotificationMetaDataBuilder(request, NotificationTypes.ACCEPTED_SENDER, NotificationTypes.NONE).build(NotificationContext.build(request));
        }
        return new ArrayList<>();
    }


    @Override
    protected void postAcceptRequest(MessageProcessingRequest request, MessageProcessingResponse response) {
        Transaction transaction = (Transaction) request.getExtraData().get(TRANSACTION_BY_REF);
        request.getModelFactory().getTransactionModel().reverse(transaction);
        request.getModelFactory().getTransactionModel().update(transaction);
    }

    private void fillBeneficiarySide(MessageProcessingRequest request, CustomerMobile customerMobile, Customer customer) {
        ProcessingRequestSide senderSide = new ProcessingRequestSide();
        senderSide.setMobile(customerMobile);
        senderSide.setCustomer(customer);
        request.setSender(senderSide);
    }

    private Long setMobileId(Transaction transaction, MessageType messageType) {
        Long mobileId = null;
        if (!messageType.getCode().equals(MessageTypeCodes.CASH_IN.getValue()))
            mobileId = transaction.getReceiverMobileId();
        else if (!messageType.getCode().equals(MessageTypeCodes.CASH_OUT.getValue()))
            mobileId = transaction.getSenderMobileId();
        return mobileId;
    }
}
