package com.progressoft.mpay.barwa.plugins.transactionreversal;

import com.progressoft.mpay.common.Reasons;
import com.progressoft.mpay.entities.EndPointOperation;
import com.progressoft.mpay.entities.MessageType;
import com.progressoft.mpay.entities.Transaction;
import com.progressoft.mpay.entities.ValidationResult;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.models.impl.psp.MessageTypeCodes;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.ProcessingUseCaseRequest;

public class CashInCashOutTransactionReversalValidator implements IValidator {


    @Override
    public ValidationResult validate(ProcessingUseCaseRequest request) {
        if (request == null)
            throw new NullArgumentException("request");

        Object extraData = request.getExtraData().get(TransactionReversalMessageProcessingUseCase.TRANSACTION_BY_REF);
        if (extraData == null)
            new ValidationResult(Reasons.TRANSACTION_NOT_FOUND, null, false);
        Transaction transaction = (Transaction) extraData;
        EndPointOperation endPointOperation = request.getModelFactory().getEndPointOperationModel().get(transaction.getOperationId());
        MessageType messageType = request.getModelFactory().getLookupsModel().get(endPointOperation.getMessageTypeId(), MessageType.class);
        if (!messageType.getCode().equals(MessageTypeCodes.CASH_IN.getValue()) && !messageType.getCode().equals(MessageTypeCodes.CASH_OUT.getValue()))
            new ValidationResult(Reasons.INVALID_OPERATION, null, false);
        request.getExtraData().put(TransactionReversalMessageProcessingUseCase.MESSAGE_TYPE, messageType);
        return new ValidationResult(Reasons.VALID, null, true);
    }
}
