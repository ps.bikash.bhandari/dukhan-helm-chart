package com.progressoft.mpay.barwa.plugins.transactionreversal;

import com.progressoft.mpay.common.OriginatorTypes;
import com.progressoft.mpay.entities.CoreRequest;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NullArgumentException;

public class TransactionReversalRequest extends CoreRequest {
    private static final String TRANSACTION_REF_NUMBER = "transactionRef";

    private String transactionReferenceId;


    public TransactionReversalRequest() {
        // Default
    }

    public TransactionReversalRequest(CoreRequest request) {
        if (request == null)
            throw new NullArgumentException("request");

        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setTenant(request.getTenant());
        setChecksum(request.getChecksum());
        setExtraData(request.getExtraData());
    }

    public static TransactionReversalRequest parseMessage(CoreRequest request) throws MessageParsingException {
        if (request == null)
            return null;
        TransactionReversalRequest message = new TransactionReversalRequest(request);
        if (isBlank(message.getSender()))
            throw new MessageParsingException(SENDER_KEY);

        if (message.getSenderType() == null || !message.getSenderType().trim().equals(OriginatorTypes.CORPORATE.getValue()))
            throw new MessageParsingException(SENDER_TYPE_KEY);

        message.setTransactionReferenceId(message.getValue(TRANSACTION_REF_NUMBER));
        if (isBlank(message.getTransactionReferenceId()))
            throw new MessageParsingException(TRANSACTION_REF_NUMBER);
        return message;
    }

    public String getTransactionReferenceId() {
        return transactionReferenceId;
    }

    public void setTransactionReferenceId(String transactionReferenceId) {
        this.transactionReferenceId = transactionReferenceId;
    }


}