package com.progressoft.mpay.barwa.plugins.moneytransferp2p;

import com.progressoft.mpay.common.OriginatorTypes;
import com.progressoft.mpay.entities.CoreRequest;
import com.progressoft.mpay.exceptions.MessageParsingException;
import com.progressoft.mpay.exceptions.NullArgumentException;

public class P2PRequest extends CoreRequest {
    private static final String RECIPIENT_NAME = "recipientName";

    private String recipientName;

    public P2PRequest(CoreRequest request) {
        if (request == null)
            throw new NullArgumentException("request");

        setOperation(request.getOperation());
        setSender(request.getSender());
        setSenderType(request.getSenderType());
        setDeviceId(request.getDeviceId());
        setLang(request.getLang());
        setMsgId(request.getMsgId());
        setPin(request.getPin());
        setAmount(request.getAmount());
        setReceiver(request.getReceiver());
        setReceiverType(request.getReceiverType());
        setSenderAccount(request.getSenderAccount());
        setReceiverAccount(request.getReceiverAccount());
        setExtraData(request.getExtraData());
        setTenant(request.getTenant());
        setChecksum(request.getChecksum());
        setSessionId(request.getSessionId());
        setNotes(request.getNotes());
    }

    public static P2PRequest parseMessage(CoreRequest request) {
        if (request == null)
            return null;

        P2PRequest message = new P2PRequest(request);
        if (message.getSender() == null || message.getSender().length() == 0)
            throw new MessageParsingException(SENDER_KEY);
        if (message.getSenderType() == null || !message.getSenderType().trim().equals(OriginatorTypes.MOBILE.getValue()))
            throw new MessageParsingException(SENDER_TYPE_KEY);
        message.setRecipientName(message.getValue(RECIPIENT_NAME));
        return message;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }
}
