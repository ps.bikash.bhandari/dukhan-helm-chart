package com.progressoft.mpay.barwa.plugins.moneytransferp2p;

import com.progressoft.mpay.common.MPClearIntegrationTypes;
import com.progressoft.mpay.barwa.plugins.moneytransferp2p.integrations.P2PIntegrationProcessingUseCase;
import com.progressoft.mpay.usecases.messages.FinancialMessageProcessingUseCaseFactory;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingUseCase;
import com.progressoft.mpay.usecases.messages.MessageProcessingUseCase;

public class P2PMessageProcessingUseCaseFactory extends FinancialMessageProcessingUseCaseFactory {


    @Override
    public MessageProcessingUseCase getMessageProcessingUseCase() {
        return new P2PMessageProcessingUseCase();
    }

    @Override
    public IntegrationProcessingUseCase getIntegrationProcessingUseCase(MPClearIntegrationTypes type) {
        return new P2PIntegrationProcessingUseCase(type);
    }
}
