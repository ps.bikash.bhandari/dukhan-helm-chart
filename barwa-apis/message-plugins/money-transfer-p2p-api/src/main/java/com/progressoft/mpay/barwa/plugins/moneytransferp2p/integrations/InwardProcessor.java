package com.progressoft.mpay.barwa.plugins.moneytransferp2p.integrations;

import com.progressoft.mpay.common.MPClearMetaDataKeys;
import com.progressoft.mpay.common.OriginatorTypes;
import com.progressoft.mpay.common.TransactionTypes;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.FinancialCalculationsHandler;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingRequest;
import com.progressoft.mpay.usecases.messages.ProcessingRequestSideMobileBuilder;
import com.progressoft.mpay.usecases.messages.TransactionConfigLoader;
import com.progressoft.mpay.usecases.messages.validators.*;
import com.progressoft.mpay.usecases.transactions.TransactionCreatorFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InwardProcessor extends IntegrationProcessor {

    @Override
    public void preAcceptRequest(IntegrationProcessingRequest request) {
        String comments = (String) request.getMetaData().getExtraData().get(MPClearMetaDataKeys.DESCRIPTION);
        String reference = (String) request.getMetaData().getExtraData().get(MPClearMetaDataKeys.REFERENCE);
        request.setTransaction(TransactionCreatorFactory.create(request.getTransactionType()).createTransaction(request, comments, reference));
    }

    @Override
    public void preProcess(IntegrationProcessingRequest request) {
        OriginatorTypes receiverType = (OriginatorTypes) request.getMetaData().getExtraData().get(MPClearMetaDataKeys.RECEIVER_TYPE);
        String receiverInfo = (String) request.getMetaData().getExtraData().get(MPClearMetaDataKeys.RECEIVER_INFO);
        String receiverAccount = (String) request.getMetaData().getExtraData().get(MPClearMetaDataKeys.RECEIVER_ACCOUNT);
        handleInward(request, (String) request.getMetaData().getExtraData().get(MPClearMetaDataKeys.SENDER_INFO));
        request.setOriginalMPclearMessage(request.getModelFactory().getMPClearIntegrationMessageModel().get(request.getMpclearMessage().getMessageId(), request.getTenant()));
        if (request.getOriginalMPclearMessage() != null)
            return;
        request.setAmount((BigDecimal) request.getMetaData().getExtraData().get(MPClearMetaDataKeys.AMOUNT));
        request.setTransactionType(TransactionTypes.DIRECT_CREDIT);
        request.setReceiver(new ProcessingRequestSideMobileBuilder().setModelFactory(request.getModelFactory()).setInfo(receiverInfo).setInfoType(receiverType.getValue())
                .setRegistrationId(receiverAccount).setTenant(request.getTenant()).build());
        FinancialCalculationsHandler.calculate(request, request.getOperation());
        request.setTransactionConfig(TransactionConfigLoader.load(request));
    }

    @Override
    public List<IValidator> getValidators() {
        List<IValidator> validators = new ArrayList<>();
        validators.add(new DuplicateRequestValidator());
        validators.add(new ReceiverMobileValidator());
        validators.add(new ReceiverMobileAccountValidator());
        validators.add(new ReceiverCustomerValidator());
        validators.add(new TransactionConfigValidator());
        return validators;
    }

    @Override
    public String getPaymentType() {
        return "Transactions.P2P.Responses.Receiver";
    }

}
