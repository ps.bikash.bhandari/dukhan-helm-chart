package com.progressoft.mpay.barwa.plugins.moneytransferp2p;

import com.progressoft.mpay.common.*;
import com.progressoft.mpay.entities.CoreRequest;
import com.progressoft.mpay.entities.NotificationMetaData;
import com.progressoft.mpay.exceptions.NotSupportedException;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.FinancialMessageProcessingUseCase;
import com.progressoft.mpay.usecases.messages.MessageProcessingRequest;
import com.progressoft.mpay.usecases.messages.MessageProcessingResponse;
import com.progressoft.mpay.usecases.messages.notifications.NotificationContext;
import com.progressoft.mpay.usecases.messages.notifications.NotificationMetaDataBuilder;
import com.progressoft.mpay.usecases.messages.validators.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class P2PMessageProcessingUseCase extends FinancialMessageProcessingUseCase {

    public static final String P2P_ONUS = "0";

    @Override
    protected CoreRequest getCoreRequest(MessageProcessingRequest request) {
        return P2PRequest.parseMessage(request.getCoreRequest());
    }

    @Override
    protected boolean loadBySessionId() {
        return false;
    }

    @Override
    protected OriginatorTypes getSenderType() {
        return OriginatorTypes.MOBILE;
    }

    @Override
    protected OriginatorTypes getReceiverType() {
        return OriginatorTypes.MOBILE;
    }

    @Override
    protected List<IValidator> getValidators(OriginatorTypes senderType) {
        if (senderType == null || !senderType.equals(OriginatorTypes.MOBILE))
            throw new NotSupportedException("senderType: " + Objects.requireNonNull(senderType).getValue());
        List<IValidator> validators = new ArrayList<>();
        validators.add(new LanguageValidator());
        validators.add(new RequestTypeValidator());
        validators.add(new SenderMobileValidator());
        validators.add(new SenderMobileAccountValidator());
        validators.add(new SenderCustomerValidator());
        validators.add(new ReceiverMobileValidator());
        validators.add(new ReceiverMobileAccountValidator());
        validators.add(new ReceiverCustomerValidator());
        validators.add(new TransactionConfigValidator());
        validators.add(new LimitsValidator());
        return validators;
    }

    @Override
    protected List<NotificationMetaData> createNotifications(MessageProcessingRequest request, ProcessingStatuses processingStatusCode) {
        if (processingStatusCode.equals(ProcessingStatuses.ACCEPTED))
            return new NotificationMetaDataBuilder(request, NotificationTypes.ACCEPTED_SENDER, NotificationTypes.ACCEPTED_RECEIVER).build(NotificationContext.build(request));
        return new ArrayList<>();
    }


    @Override
    protected void postAcceptRequest(MessageProcessingRequest request, MessageProcessingResponse response) {
        super.postAcceptRequest(request, response);
        P2PRequest message = (P2PRequest) request.getCoreRequest();
        if (request.getReceiver().getMobile() != null)
            response.getTransaction().setReceiverName(request.getReceiver().getCustomer().getFullName());
        else if (message.getRecipientName() != null && !message.getRecipientName().isEmpty())
            response.getTransaction().setReceiverName(message.getRecipientName());

        if (isP2pOnus(request)) {
            response.setProcessingStatus(ProcessingStatuses.ACCEPTED);
            response.getCoreMessage().setProcessingStatus(ProcessingStatuses.ACCEPTED);
            response.getTransaction().setProcessingStatus(ProcessingStatuses.ACCEPTED);
            response.getResponse().setStatusCode(String.valueOf(ProcessingStatuses.ACCEPTED.getValue()));
        }
    }

    private boolean isP2pOnus(MessageProcessingRequest request) {
        return request.getModelFactory().getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.IS_P2P_ONUS.getValue(),
                request.getTenant()).getValue().equalsIgnoreCase(P2P_ONUS) && request.getTransaction().getDirection().equals(TransactionDirections.ONUS);
    }
}