package com.progressoft.mpay.barwa.plugins.moneytransferp2p.integrations;

import com.progressoft.mpay.common.MPClearIntegrationTypes;
import com.progressoft.mpay.exceptions.NotSupportedException;

public class IntegrationProcessorFactory {
    private IntegrationProcessorFactory() {

    }

    public static IntegrationProcessor get(MPClearIntegrationTypes type) {
        if (MPClearIntegrationTypes.ONLINE_FINANCIAL_REQUEST.equals(type))
            return new InwardProcessor();
        else if (MPClearIntegrationTypes.ONLINE_FINANCIAL_RESPONSE.equals(type))
            return new ResponseProcessor();
        else
            throw new NotSupportedException("Not Supported Type: " + type);
    }
}
