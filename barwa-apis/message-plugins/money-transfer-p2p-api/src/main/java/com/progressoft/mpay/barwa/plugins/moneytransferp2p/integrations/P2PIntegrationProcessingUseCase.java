package com.progressoft.mpay.barwa.plugins.moneytransferp2p.integrations;

import com.progressoft.mpay.common.*;
import com.progressoft.mpay.entities.MPClearIntegrationMetaData;
import com.progressoft.mpay.entities.NotificationMetaData;
import com.progressoft.mpay.entities.Reason;
import com.progressoft.mpay.entities.Transaction;
import com.progressoft.mpay.models.IModelFactory;
import com.progressoft.mpay.models.transaction.TransactionModel;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingRequest;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingResponse;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingUseCase;
import com.progressoft.mpay.usecases.messages.notifications.NotificationContext;
import com.progressoft.mpay.usecases.messages.notifications.NotificationMetaDataBuilder;

import java.util.ArrayList;
import java.util.List;

public class P2PIntegrationProcessingUseCase extends IntegrationProcessingUseCase {

    private final IntegrationProcessor integrationProcessor;

    public P2PIntegrationProcessingUseCase(MPClearIntegrationTypes type) {
        integrationProcessor = IntegrationProcessorFactory.get(type);
    }

    @Override
    protected void preProcessRequest(IntegrationProcessingRequest request) {
        integrationProcessor.preProcess(request);
    }

    @Override
    protected List<IValidator> getValidators(MPClearIntegrationTypes type) {
        return integrationProcessor.getValidators();
    }

    @Override
    public IntegrationProcessingResponse process(IntegrationProcessingRequest request) {
        if (request.getTransaction() != null && request.getTransaction().getDirection().getValue() == TransactionDirections.INWARD.getValue())
            return responseForInwardResponse(request, request.getTransaction());
        return super.process(request);
    }


    @Override
    protected List<NotificationMetaData> createNotifications(IntegrationProcessingRequest request, ProcessingStatuses processingStatusCode) {
        if (request.getTransaction() == null) return new ArrayList<>();
        if (request.getTransaction().getDirection().getValue() == TransactionDirections.ONUS.getValue())
            return new ArrayList<>();
        if (processingStatusCode.equals(ProcessingStatuses.ACCEPTED))
            return new NotificationMetaDataBuilder(request, NotificationTypes.ACCEPTED_SENDER, NotificationTypes.ACCEPTED_RECEIVER).build(NotificationContext.build(request));
        else if (isInward(request, processingStatusCode))
            return new NotificationMetaDataBuilder(request, NotificationTypes.NONE, NotificationTypes.ACCEPTED_RECEIVER).build(NotificationContext.build(request));
        return new ArrayList<>();
    }

    @Override
    protected void preAcceptRequest(IntegrationProcessingRequest request) {
        integrationProcessor.preAcceptRequest(request);
    }

    @Override
    protected void postAcceptRequest(IntegrationProcessingRequest request, IntegrationProcessingResponse response) {
        if (request.getIntegrationMessageType().isRequest())
            createMPClearResponse(request, response);
    }


    @Override
    protected void postRejectRequest(IntegrationProcessingRequest request, IntegrationProcessingResponse response) {
        if (request.getIntegrationMessageType().isRequest()) {
            createMPClearResponse(request, response);
            Reason reason = request.getModelFactory().getLookupsModel().get(response.getReasonCode().getValue(), Reason.class);
            response.getMpClearResponseMetaData().getExtraData().put(MPClearMetaDataKeys.MP_CLEAR_REASON_CODE, reason.getMpClearCode());
        }
    }


    protected void createMPClearResponse(IntegrationProcessingRequest request, IntegrationProcessingResponse response) {
        if (!request.getIntegrationMessageType().isRequest())
            return;
        MPClearIntegrationMetaData data = new MPClearIntegrationMetaData();
        long id = request.getTransaction() == null ? request.getModelFactory().getMPClearIntegrationMessageModel().getNextMessageId() : request.getTransaction().getId();
        data.setType(MPClearIntegrationTypes.ONLINE_FINANCIAL_RESPONSE);
        data.getExtraData().put(MPClearMetaDataKeys.ID, id);
        data.getExtraData().put(MPClearMetaDataKeys.PROCESSING_CODE, request.getMetaData().getExtraData().get(MPClearMetaDataKeys.PROCESSING_CODE));
        data.getExtraData().put(MPClearMetaDataKeys.CORRELATION_ID, getNextMPClearMessageId(request.getModelFactory(), request.getModelFactory().getSystemConfigurationModel().getSystemConfig(SystemConfigurationKeys.DEFAULT_BANK_CODE.getValue(), request.getTenant()).getValue()));
        data.getExtraData().put(MPClearMetaDataKeys.ORIGINAL_MESSAGE_ID, request.getMetaData().getExtraData().get(MPClearMetaDataKeys.ORIGINAL_MESSAGE_ID));
        data.getExtraData().put(MPClearMetaDataKeys.DATE, request.getModelFactory().getDateUtilsModel().getCurrentTimestamp());
        data.getExtraData().put(MPClearMetaDataKeys.CURRENCY, request.getMetaData().getExtraData().get(MPClearMetaDataKeys.CURRENCY));
        data.getExtraData().put(MPClearMetaDataKeys.PROCESSING_STATUS, response.getProcessingStatus());
        data.getExtraData().put(MPClearMetaDataKeys.REASON_CODE, response.getReasonCode());
        data.getExtraData().put(MPClearMetaDataKeys.REASON_DESCRIPTION, response.getReasonDescription());
        if (request.getReceiver().getCustomer() != null) {
            data.getExtraData().put(MPClearMetaDataKeys.RECEIVER_NAME, request.getReceiver().getCustomer().getFullName());
            data.getExtraData().put(MPClearMetaDataKeys.RECEIVER_ADDRESS, request.getReceiver().getCustomer().getAddress());
        }
        if (request.getMessage() != null)
            data.getExtraData().put(MPClearMetaDataKeys.REFERENCE, String.valueOf(response.getCoreMessage().getId()));
        data.getExtraData().put(MPClearMetaDataKeys.ORIGINAL_ID, request.getMpclearMessage().getId());
        if (request.getTransaction() != null)
            data.getExtraData().put(MPClearMetaDataKeys.INWARD_TRANSACTION_ID, request.getTransaction().getId());
        data.getExtraData().put(MPClearMetaDataKeys.SENDER_PSP, request.getMetaData().getExtraData().get(MPClearMetaDataKeys.SENDER_PSP));
        data.getExtraData().put(MPClearMetaDataKeys.RECEIVER_PSP, request.getMetaData().getExtraData().get(MPClearMetaDataKeys.RECEIVER_PSP));
        response.setMpClearResponseMetaData(data);
    }

    protected String getNextMPClearMessageId(IModelFactory modelFactory, String pspShortName) {
        return pspShortName + String.format("%012d", modelFactory.getMPClearIntegrationMessageModel().getNextMessageId());
    }

    private IntegrationProcessingResponse responseForInwardResponse(IntegrationProcessingRequest request, Transaction transaction) {
        ProcessingStatuses processingStatuses = (ProcessingStatuses) request.getMetaData().getExtraData().get(MPClearMetaDataKeys.PROCESSING_STATUS);
        if (processingStatuses.getValue() == ProcessingStatuses.ACCEPTED.getValue()) {
            TransactionModel transactionModel = request.getModelFactory().getTransactionModel();
            transactionModel.postJournalVoucher(transaction);
            transaction.setProcessingStatus(ProcessingStatuses.ACCEPTED);
            transactionModel.update(transaction);
        } else {
            TransactionModel transactionModel = request.getModelFactory().getTransactionModel();
            transactionModel.postJournalVoucher(transaction);
            transaction.setProcessingStatus(ProcessingStatuses.REJECTED);
            transaction.setReasonId(Reasons.REJECTED_BY_NATIONAL_SWITCH);
            transaction.setReasonDescription((String) request.getMetaData().getExtraData().get(MPClearMetaDataKeys.REASON_DESCRIPTION));
            transactionModel.update(transaction);
        }
        return IntegrationProcessingResponse.build(request.getResponseMetaData(), request, ProcessingStatuses.ACCEPTED, Reasons.VALID, null);
    }


    private boolean isInward(IntegrationProcessingRequest request, ProcessingStatuses processingStatusCode) {
        return processingStatusCode.equals(ProcessingStatuses.PENDING) && request.getTransaction().getDirection().getValue() == TransactionDirections.INWARD.getValue();
    }

}