package com.progressoft.mpay.barwa.plugins.moneytransferp2p.integrations;

import com.progressoft.mpay.common.MPClearMetaDataKeys;
import com.progressoft.mpay.common.ProcessingStatuses;
import com.progressoft.mpay.common.Reasons;
import com.progressoft.mpay.entities.MPClearIntegrationMetaData;
import com.progressoft.mpay.entities.ValidationResult;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingRequest;
import com.progressoft.mpay.usecases.messages.ProcessingUseCaseRequest;

public class MPClearResponseValidator implements IValidator {
	@Override
	public ValidationResult validate(ProcessingUseCaseRequest request) {
		if (request == null)
			throw new NullArgumentException("request");
		IntegrationProcessingRequest integrationRequest = (IntegrationProcessingRequest) request;
		if (ProcessingStatuses.ACCEPTED.equals(integrationRequest.getMetaData().getExtraData().get(MPClearMetaDataKeys.PROCESSING_STATUS)))
			return ValidationResult.getValid();
		return new ValidationResult(Reasons.REJECTED_BY_NATIONAL_SWITCH, getReasonDescription(integrationRequest.getMetaData()), false);
	}

	private String getReasonDescription(MPClearIntegrationMetaData metaData) {
		return  metaData.getExtraData().get(MPClearMetaDataKeys.REASON_CODE) + "-" + (String) metaData.getExtraData().get(MPClearMetaDataKeys.REASON_DESCRIPTION);
	}
}
