package com.progressoft.mpay.barwa.plugins.moneytransferp2p.integrations;

import com.progressoft.mpay.common.TransactionDirections;
import com.progressoft.mpay.exceptions.NullArgumentException;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingRequest;

import java.util.ArrayList;
import java.util.List;

public class ResponseProcessor extends IntegrationProcessor {


    @Override
    public void preProcess(IntegrationProcessingRequest request) {
        if (request == null)
            throw new NullArgumentException("request");
        if (isInward(request))
            return;
        request.setAmount(request.getTransaction().getOriginalAmount());
        request.getSender().setMobile(request.getModelFactory().getCustomerMobileModel().get(request.getTransaction().getSenderMobileId()));
        request.getSender().setCustomer(request.getModelFactory().getCustomerModel().get(request.getSender().getMobile().getCustomerId()));
        request.getSender().setMobileAccount(request.getModelFactory().getMobileAccountModel().get(request.getTransaction().getSenderMobileAccountId()));
        request.getSender().setAccount(request.getModelFactory().getAccountModel().get(request.getSender().getMobileAccount().getAccountId()));
        request.getSender().setCharge(request.getTransaction().getSenderCharge());
        request.getSender().setTax(request.getTransaction().getSenderTax());
        request.getSender().setInfo(request.getTransaction().getSenderInfo());

        request.getReceiver().setInfo(request.getTransaction().getReceiverInfo());
        if (request.getTransaction().getReceiverMobileId() == null)
            return;
        request.getReceiver().setMobile(request.getModelFactory().getCustomerMobileModel().get(request.getTransaction().getReceiverMobileId()));
        request.getReceiver().setCustomer(request.getModelFactory().getCustomerModel().get(request.getReceiver().getMobile().getCustomerId()));
        request.getReceiver().setMobileAccount(request.getModelFactory().getMobileAccountModel().get(request.getTransaction().getReceiverMobileAccountId()));
        request.getReceiver().setAccount(request.getModelFactory().getAccountModel().get(request.getReceiver().getMobileAccount().getAccountId()));
        request.getReceiver().setCharge(request.getTransaction().getReceiverCharge());
        request.getReceiver().setTax(request.getTransaction().getReceiverTax());

    }

    @Override
    public List<IValidator> getValidators() {
        List<IValidator> validators = new ArrayList<>();
        validators.add(new MPClearResponseValidator());
        return validators;
    }

    @Override
    public String getPaymentType() {
        return "Transactions.P2P.Responses.Interbank";
    }

    private boolean isInward(IntegrationProcessingRequest request) {
        return request.getTransaction().getDirection().getValue() == TransactionDirections.INWARD.getValue();
    }
}
