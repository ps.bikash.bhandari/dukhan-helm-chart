package com.progressoft.mpay.barwa.plugins.moneytransferp2p.integrations;

import com.progressoft.mpay.common.TransactionDirections;
import com.progressoft.mpay.usecases.IValidator;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingRequest;
import com.progressoft.mpay.usecases.messages.ProcessingRequestSideMPClearBuilder;
import com.progressoft.mpay.usecases.messages.ProcessingUseCaseRequest;

import java.util.List;

public abstract class IntegrationProcessor {
	public abstract void preProcess(IntegrationProcessingRequest request);

	public abstract List<IValidator> getValidators();

	public abstract String getPaymentType();

	public void preAcceptRequest(IntegrationProcessingRequest request) {

	}

	public void preRejectRequest(IntegrationProcessingRequest request) {

	}

	protected void handleInward(ProcessingUseCaseRequest request, String senderInfo) {
		request.setSender(new ProcessingRequestSideMPClearBuilder().setModelFactory(request.getModelFactory()).setTenant(request.getTenant()).setInfo(senderInfo).build());
		request.setDirection(TransactionDirections.INWARD);
	}

}
