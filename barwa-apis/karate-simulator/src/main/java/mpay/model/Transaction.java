package mpay.model;

public class Transaction {

    private Long processingStatusId;
    private Long reasonId;
    private Long id;

    public Transaction(Long processingStatusId, Long reasonId, Long id) {
        this.processingStatusId = processingStatusId;
        this.reasonId = reasonId;
        this.id = id;
    }

    public Long getProcessingStatusId() {
        return processingStatusId;
    }

    public void setProcessingStatusId(Long processingStatusId) {
        this.processingStatusId = processingStatusId;
    }

    public Long getReasonId() {
        return reasonId;
    }

    public void setReasonId(Long reasonId) {
        this.reasonId = reasonId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
