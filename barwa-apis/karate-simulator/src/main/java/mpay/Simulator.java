package mpay;

import mpay.model.Transaction;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("simulator")
public class Simulator {

    public static final String FALSE = "0";
    public static final String TRUE = "1";
    public static final int ACCEPTED = 1;

    @GetMapping(value = "/test/checkTransactionIsPosted")
    public String checkTransactionIsPosted(@RequestParam("msgId") String msgId) {
        JDBCConnector connector = new JDBCConnector();
        Transaction transaction = connector.checkTransactionIsPosted(msgId);
        if (transaction == null)
            return FALSE;
        if (transaction.getProcessingStatusId() != ACCEPTED && transaction.getReasonId() != ACCEPTED)
            return FALSE;
        if (connector.checkIsJvsPosted(transaction.getId()))
            return TRUE;
        return FALSE;
    }


    @GetMapping(value = "/test/checkInwardTransactionIsPosted")
    public String checkInwardTransactionIsPosted(@RequestParam("msgId") String msgId) {
        JDBCConnector connector = new JDBCConnector();
        Transaction transaction = connector.checkInwardTransactionIsPosted(msgId);
        if (transaction == null)
            return FALSE;
        if (transaction.getProcessingStatusId() != ACCEPTED && transaction.getReasonId() != ACCEPTED)
            return FALSE;
        if (connector.checkIsJvsPosted(transaction.getId()))
            return TRUE;
        return FALSE;
    }


    @GetMapping(value = "/test/hasMpClearMessageLog")
    public String hasMpClearMessageLog(@RequestParam("msgId") String msgId) {
        JDBCConnector connector = new JDBCConnector();
        if (connector.hasMpClearMessageLog(msgId)) return TRUE;
        return FALSE;
    }

}
