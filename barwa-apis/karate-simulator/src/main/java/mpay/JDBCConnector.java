package mpay;


import mpay.model.Transaction;

import java.sql.*;

public class JDBCConnector {
    static final String JDBC_DRIVER = "oracle.jdbc.OracleDriver";
    static final String DB_URL = System.getenv("db_url");
    static final String USER = System.getenv("db_user");
    static final String PASS = System.getenv("db_password");

    public Transaction checkTransactionIsPosted(String msgId) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            String sql = "select id,processingstatusid,reasonid from mpay_transactions where refmessageid in "
                    + "(select id from mpay_mpaymessages where mpay_mpaymessages.messageid='" + msgId + "')";

            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next())
                return new Transaction(rs.getLong(2), rs.getLong(3), rs.getLong(1));
            return null;
        } catch (SQLException | ClassNotFoundException se) {
            se.printStackTrace();
        } finally {
            closeConnection(conn, stmt);
        }
        return null;
    }


    public Transaction checkInwardTransactionIsPosted(String msgId) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            String sql = "select id,processingstatusid,reasonid from mpay_transactions where mpclearrequestid ='" + msgId + "' ";

            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next())
                return new Transaction(rs.getLong(2), rs.getLong(3), rs.getLong(1));
            return null;
        } catch (SQLException | ClassNotFoundException se) {
            se.printStackTrace();
        } finally {
            closeConnection(conn, stmt);
        }
        return null;
    }

    public boolean checkIsJvsPosted(Long transactionId) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(
                    "select isposted from mpay_jvdetails where reftrsansactionid='" + transactionId + "'");
            int count = 0;
            while (rs.next()) {
                if (!rs.getString(1).equals("1"))
                    return false;
                count++;
            }
            return count == 2;
        } catch (SQLException | ClassNotFoundException se) {
            se.printStackTrace();
        } finally {
            closeConnection(conn, stmt);
        }
        return false;
    }

    public boolean hasMpClearMessageLog(String msgId) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql = "select COUNT(*) from mpay_mpclearintegmsglogs where refmessageid in (select id from mpay_mpaymessages where mpay_mpaymessages.messageid='"
                    + msgId + "')";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next())
                return rs.getInt(1) == 2;
        } catch (SQLException | ClassNotFoundException se) {
            se.printStackTrace();
        } finally {
            closeConnection(conn, stmt);
        }
        return false;
    }


    private void closeConnection(Connection conn, Statement stmt) {
        try {
            if (stmt != null)
                conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
        try {
            if (conn != null)
                conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }
}