package mpay.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ExtraDataModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @SerializedName("key")
    private String key;
    @SerializedName("value")
    private String value;

    public ExtraDataModel() {
    }

    public ExtraDataModel(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int describeContents() {
        return 0;
    }
}
