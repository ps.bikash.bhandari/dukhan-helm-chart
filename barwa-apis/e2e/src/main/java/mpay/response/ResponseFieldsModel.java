package mpay.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseFieldsModel {
    @SerializedName("errorCd")
    private String errorCd;
    @SerializedName("desc")
    private String desc;
    @SerializedName("ref")
    private long ref;
    @SerializedName("statusCode")
    private String statusCode;
    @SerializedName("extraData")
    private List<ExtraDataModel> extraData;


    public String getErrorCd() {
        return errorCd;
    }

    public void setErrorCd(String errorCd) {
        this.errorCd = errorCd;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getRef() {
        return ref;
    }

    public void setRef(long ref) {
        this.ref = ref;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<ExtraDataModel> getExtraData() {
        return extraData;
    }

    public void setExtraData(List<ExtraDataModel> extraData) {
        this.extraData = extraData;
    }

}