package mpay.response;

import com.google.gson.annotations.SerializedName;

public class ResponseModel {

    @SerializedName("response")
    private ResponseFieldsModel response;

    public ResponseFieldsModel getResponse() {
        return response;
    }

    public void setResponse(ResponseFieldsModel response) {
        this.response = response;
    }
}