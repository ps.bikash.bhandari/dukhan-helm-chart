package mpay;

import com.google.gson.Gson;
import com.opensymphony.util.GUID;
import mpay.response.ResponseModel;

import java.util.Random;


public class Helper {

    public static final String SUCCESS_CODE = "00";
    public static final String TRUE = "1";

    public static boolean isSuccess(String response) {
        ResponseModel responseModel = new Gson().fromJson(response, ResponseModel.class);
        return responseModel.getResponse().getErrorCd().equalsIgnoreCase(SUCCESS_CODE);
    }

    public static String msgId() {
        return GUID.generateGUID();
    }

    public static boolean isTrue(String response) {
        sleep(3000);
        return response.equalsIgnoreCase(TRUE);
    }


    public static boolean wait(Long time) {
        sleep(time);
        return true;
    }

    public static String mobileNumber() {
        Random generator = new Random();
        int set1 = generator.nextInt(9);
        if (set1 == 0 || set1 == 1 || set1 == 3)
            set1 = set1 + 4;
        int set2 = generator.nextInt(64) + 10;
        int set3 = generator.nextInt(8999) + 1000;
        return "009742" + set1 + set2 + set3;
    }

    public static String alias() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 5;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString + new Random().nextInt(50) + "@barwa";
    }

    public static String idNumber() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 5;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString + new Random().nextInt(50);
    }

    private static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println(mobileNumber());//12
        System.out.println("=======================");
        System.out.println(alias());
        System.out.println("=======================");
        System.out.println(idNumber());
        System.out.println("=======================");
    }

}
