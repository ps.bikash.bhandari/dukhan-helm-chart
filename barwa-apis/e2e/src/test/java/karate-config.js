function fn() {
     //example Karate Simulator Url = http://localhost:4545
    var systemPath = java.lang.System.getenv('SPRING_URL');
    //example Karate Simulator Url = http://localhost:9090/simulator
    var simPath = java.lang.System.getenv('SIMULATOR_URL');
    //example MPC Url = http://localhost:5454/MPC
    var shortCircuitPath = java.lang.System.getenv('MPCLEAR_SHORT_CIRCUIT_URL');
    var config = {
        baseUrl: systemPath,
        simulatorUrl: simPath,
        mpClearShortCircuitPath:shortCircuitPath,
        senderMobile: '0097412234500',
        receiverMobile: '0097412234500',
        p2pReceiverMobile: '0097411000081',
        receiverCashOutService: 'DUKHCO',
        senderCashInService: 'DUKHCI',
        receiverOutwardMobile: '0097466426636',
        alias:'laptop3@DUKH',
        idNumber:'laptop0002',
        accountNumber:'2921218077318',
        cashInAmount: '50',
        cashOutAmount: '10',
        p2pAmount: '10'
    };
    karate.configure('connectTimeout', 5000);
    karate.configure('readTimeout', 1000000);
    return config;
}