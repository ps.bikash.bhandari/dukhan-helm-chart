Feature:customer miniStatement

  Background:
    * url baseUrl
    * def helper = Java.type('com.progressoft.mpay.Helper')

  Scenario: customer miniStatement return success
    * def msgId = helper.msgId()
    * string body = read('classpath:qatar/requests/mini-statament.json')
    Given path 'processMessage/v1'
    And param token = 'test'
    And request body
    When method POST
    Then assert helper.isSuccess(response)
    Then status 200