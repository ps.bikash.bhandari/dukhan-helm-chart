Feature:Customer Fees Calculation

  Background:
    * url baseUrl
    * def helper = Java.type('com.progressoft.mpay.Helper')

  Scenario: customer fees calculation
    * def msgId = helper.msgId()
    * string body = read('classpath:qatar/requests/fees-calculation.json')
    Given path 'processMessage/v1'
    And param token = 'test'
    And request body
    When method POST
    Then assert helper.isSuccess(response)
    Then status 200