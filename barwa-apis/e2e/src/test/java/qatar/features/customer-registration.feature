Feature: customer-registration

  Background:
    * url baseUrl
    * def helper = Java.type('com.progressoft.mpay.Helper')

  Scenario: customer registration  and return register customer successfully
    * def idNumber = helper.idNumber()
    * def alias = helper.alias()
    * def mobileNumber = helper.mobileNumber()
    * def msgId = helper.msgId()
    * string body = read('classpath:qatar/requests/customer-registration.json')


    Given path 'processMessage/v1'
    And param token = 'test'
    And request body
    When method POST
    Then assert helper.isSuccess(response)
    Then status 200