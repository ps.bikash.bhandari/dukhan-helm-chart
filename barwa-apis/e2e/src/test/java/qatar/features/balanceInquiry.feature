Feature:Balance Inquiry

  Background:
    * url baseUrl
    * def helper = Java.type('com.progressoft.mpay.Helper')

  Scenario: customer balance inquiry return success
    * def msgId = helper.msgId()
    * string body = read('classpath:qatar/requests/balance-inquiry.json')
    Given path 'processMessage/v1'
    And param token = 'test'
    And request body
    When method POST
    Then assert helper.isSuccess(response)
    Then status 200