Feature: customer-cash-out

  Background:
    * url baseUrl
    * def helper = Java.type('com.progressoft.mpay.Helper')

  Scenario: customer cash out and return success
    * def msgId = helper.msgId()
    * string body = read('classpath:qatar/requests/customer-cash-out.json')
    Given path 'processMessage/v1'
    And param token = 'test'
    And request body
    When method POST
    Then assert helper.isSuccess(response)
    Then status 200
    * url simulatorUrl
    Given path 'test/checkTransactionIsPosted'
    And param msgId = msgId
    When method GET
    Then assert helper.isTrue(response)
    Then status 200
    * url simulatorUrl
    Given path 'test/hasMpClearMessageLog'
    And param msgId = msgId
    When method GET
    Then assert helper.isTrue(response)
    Then status 200

