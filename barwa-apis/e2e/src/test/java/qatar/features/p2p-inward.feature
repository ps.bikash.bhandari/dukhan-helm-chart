Feature: p2p-inward

  Background:
    * url baseUrl
    * def helper = Java.type('com.progressoft.mpay.Helper')

  Scenario: p2p inward and return success
    * url mpClearShortCircuitPath
    Given path '/test/sendInward'
    When method GET
    Then def mpClearMsgId = response
    Then status 200
    Then helper.wait(10000)


    * url simulatorUrl
    Given path 'test/checkInwardTransactionIsPosted'
    And param msgId = response
    When method GET
    Then assert helper.isTrue(response)
    Then status 200
