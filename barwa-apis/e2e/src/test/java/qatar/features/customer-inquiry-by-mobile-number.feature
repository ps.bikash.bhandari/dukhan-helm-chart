Feature:Customer Inquiry By Mobile Number

  Background:
    * url baseUrl
    * def helper = Java.type('com.progressoft.mpay.Helper')

  Scenario: customer inquiry by mobile Number should return success
    * def msgId = helper.msgId()
    * string body = read('classpath:qatar/requests/customer-inquiry-by-mobile-number.json')
    Given path 'processMessage/v1'
    And param token = 'test'
    And request body
    When method POST
    Then assert helper.isSuccess(response)
    Then status 200