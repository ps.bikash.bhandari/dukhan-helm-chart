package mpclearshortcircuit;

import com.progressoft.mpadapter.mpc.envelope.MPClear;
import com.progressoft.mpadapter.mpc.envelope.MpayCertificates;
import com.progressoft.mpadapter.mpc.envelope.UnwrappedContent;
import com.progressoft.mpadapter.mpc.envelope.WrappedContent;
import com.progressoft.mpadapter.mpc.financial.model.MPClearPayment;
import com.progressoft.mpadapter.mpc.financial.model.MPClearPaymentType;
import com.progressoft.mpadapter.mpc.financial.model.MPClearResponse;
import com.progressoft.mpadapter.mpc.financial.model.Status;
import com.progressoft.mpadapter.mpc.financial.payment.MPClearPaymentParser;
import com.progressoft.mpadapter.mpc.financial.response.MPClearPaymentResponseParser;
import com.progressoft.mpadapter.mpc.financial.response.MPClearPaymentResponseSerializer;
import com.progressoft.mpadapter.mpc.registration.RegistrationParser;
import com.progressoft.mpadapter.mpc.registration.RegistrationSerializer;
import com.progressoft.mpadapter.mpc.registration.cstmrreg_04_01.Document;
import com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.ResponseStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

@Component
public class JmsConsumer {

    @Autowired
    JmsProducer jmsProducer;
    @Value("${replay.queue.name}")
    String queue;


    public static String createRandomInteger(int aStart, long aEnd, Random aRandom) {
        if (aStart > aEnd)
            throw new IllegalArgumentException("Start cannot exceed End.");
        long range = aEnd - (long) aStart + 1;
        long fraction = (long) (range * aRandom.nextDouble());
        return String.valueOf(fraction + (long) aStart);
    }

    public static MpayCertificates getMPayCertificates(String content) {
        MpayCertificates certificates = new MpayCertificates();
        certificates.setKeystorePath("keystore.jks");
        certificates.setPassword("mpayjfw");
        certificates.setAlias("mpay");
        certificates.setContent(content);
        certificates.setDate(new Date());
        return certificates;
    }

    @JmsListener(destination = "${receive.queue.name}", containerFactory = "jsaFactory")
    public void receive(String msg) {
        System.out.println("Received Message: \n" + msg);
        UnwrappedContent unwrappedContent = MPClear.unwrap(msg);
        if (unwrappedContent.getEnvelopeMessageType().name().equals("CREDIT")) {
            MPClearPaymentParser mpClearPaymentParser = new MPClearPaymentParser();
            MPClearPayment mpclearPayment = mpClearPaymentParser.parse(unwrappedContent.getMessageContent());
            mpclearPayment.setExtraData(unwrappedContent.getMessageId());
            MPClearResponse paymentResponse = getMPClearResponse(mpclearPayment);
            String pacs002 = new MPClearPaymentResponseSerializer().serialize(paymentResponse);
            WrappedContent wrap = MPClear.wrap(String.valueOf(paymentResponse.getMessageId()), "barwa", getMPayCertificates(pacs002));
            jmsProducer.send(wrap.getContent(), queue);
        }
    }

    @JmsListener(destination = "${receive.inward.queue.name}", containerFactory = "jsaFactory")
    public void receiveInward(String msg) {
        System.out.println("Received Message: \n" + msg);
        UnwrappedContent unwrappedContent = MPClear.unwrap(msg);
        MPClearResponse pacs2Response = new MPClearPaymentResponseParser().parse(unwrappedContent.getMessageContent());
        MPClearResponse paymentResponse = getMpClearResponse(pacs2Response);
        String pacs002 = new MPClearPaymentResponseSerializer().serialize(paymentResponse);
        WrappedContent wrap = MPClear.wrap(String.valueOf(paymentResponse.getMessageId()), "barwa", getMPayCertificates(pacs002));
        jmsProducer.send(wrap.getContent(), queue);
    }

    @JmsListener(destination = "${registration.queue.name}", containerFactory = "jsaFactory")
    public void registration(String msg) throws DatatypeConfigurationException {
        System.out.println("Received Message: \n" + msg);
        UnwrappedContent unwrappedContent = MPClear.unwrap(msg);

        Document registration = (Document) RegistrationParser.getInstance().parseMessage(unwrappedContent.getMessageContent());
        String msgId = registration.getCstmrAndAcntOpngReq().getMsgId().getId();
        com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.Document response = new com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.Document();
        String newMessageId = createRandomInteger(1, 1000000000, new Random());
        com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.Document.RegResp regResp = new com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.Document.RegResp();
        com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.Document.RegResp.MsgId resMsgId = new com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.Document.RegResp.MsgId();
        resMsgId.setId(newMessageId);
        resMsgId.setCreDtTm(DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar()));
        regResp.setMsgId(resMsgId);
        regResp.setOrgnlMsg(msg);
        com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.Document.RegResp.OrgnlMsgId orginalMessage = new com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.Document.RegResp.OrgnlMsgId();
        orginalMessage.setId(msgId);
        orginalMessage.setMsgTp("cstmrreg.04.01");
        regResp.setOrgnlMsgId(orginalMessage);
        com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.Document.RegResp.OrgnlMsgSts originalMessageSts = new com.progressoft.mpadapter.mpc.registration.cstmrreg_10_01.Document.RegResp.OrgnlMsgSts();
        originalMessageSts.setRsnCd("1000");
        regResp.setOrgnlMsgSts(originalMessageSts);
        originalMessageSts.setSts(ResponseStatus.ACPT);
        response.setRegResp(regResp);
        String acceptResponse = RegistrationSerializer.getInstance().serializeMessage(response);
        WrappedContent wrap = MPClear.wrap(newMessageId, "barwa", getMPayCertificates(acceptResponse));
        jmsProducer.send(wrap.getContent(), "mpc.barwa.reg.inward");
    }

    private MPClearResponse getMpClearResponse(MPClearResponse pacs2Response) {
        MPClearResponse paymentResponse = new MPClearResponse();
        paymentResponse.setMessageId(createRandomInteger(1, 1000000000, new Random()));
        paymentResponse.setOriginalMessageId(pacs2Response.getMessageId());
        paymentResponse.setOriginalMessageType(MPClearPaymentType.CREDIT_TYPE);
        paymentResponse.setCreationDateTime(pacs2Response.getCreationDateTime());
        paymentResponse.setInstructedBic(pacs2Response.getInstructedBic());
        paymentResponse.setInstructingBic(pacs2Response.getInstructingBic());
        paymentResponse.setStatus(Status.ACCEPTED_ACK);
        return paymentResponse;
    }

    private MPClearResponse getMPClearResponse(MPClearPayment mpclearPayment) {
        MPClearResponse paymentResponse = new MPClearResponse();
        paymentResponse.setMessageId(createRandomInteger(1, 1000000000, new Random()));
        paymentResponse.setOriginalMessageId(mpclearPayment.getId());
        paymentResponse.setOriginalMessageType(MPClearPaymentType.CREDIT_TYPE);
        paymentResponse.setCreationDateTime(mpclearPayment.getCreationDate());
        paymentResponse.setStatus(Status.ACCEPTED);
        paymentResponse.setInstructedBic(mpclearPayment.getSendingPSP());
        paymentResponse.setInstructingBic(mpclearPayment.getReceivingPSP());
        return paymentResponse;
    }
}