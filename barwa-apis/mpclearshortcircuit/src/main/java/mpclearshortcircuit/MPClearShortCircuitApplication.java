package mpclearshortcircuit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MPClearShortCircuitApplication {

    public static void main(String[] args) {
        SpringApplication.run(MPClearShortCircuitApplication.class, args);
    }

}
