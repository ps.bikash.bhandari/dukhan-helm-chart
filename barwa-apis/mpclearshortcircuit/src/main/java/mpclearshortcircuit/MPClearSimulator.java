package mpclearshortcircuit;


import com.progressoft.mpadapter.mpc.envelope.MPClear;
import com.progressoft.mpadapter.mpc.envelope.WrappedContent;
import com.progressoft.mpadapter.mpc.financial.model.MPClearPayment;
import com.progressoft.mpadapter.mpc.financial.model.MPClearPaymentType;
import com.progressoft.mpadapter.mpc.financial.payment.MPClearPaymentSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

import static mpclearshortcircuit.JmsConsumer.createRandomInteger;
import static mpclearshortcircuit.JmsConsumer.getMPayCertificates;

@RestController
@RequestMapping("MPC")
public class MPClearSimulator {

    @Autowired
    JmsProducer jmsProducer;

    @Value("${payment.inward.queue.name}")
    String queue;

    @GetMapping(value = "/test/sendInward")
    public String sendInward() {
        MPClearPaymentType messageType = MPClearPaymentType.CREDIT_TYPE;
        MPClearPayment payment = getInwardMPClearPayment(messageType);
        String pacs008 = new MPClearPaymentSerializer().serialize(payment);
        WrappedContent wrap = MPClear.wrap(payment.getId(), "barwa", getMPayCertificates(pacs008));
        jmsProducer.send(wrap.getContent(), queue);
        System.out.println("***********mpclearMsgId**********" + payment.getId());
        return payment.getId();
    }

    private MPClearPayment getInwardMPClearPayment(MPClearPaymentType messageType) {
        MPClearPayment payment = new MPClearPayment();
        payment.setId(createRandomInteger(1, 1000000000, new Random()));
        payment.setAmount(new BigDecimal("5").setScale(2, RoundingMode.DOWN));
        payment.setCreationDate(LocalDateTime.now());
        payment.setCurrency("QAR");
        payment.setMessageType(messageType);
        payment.setSettlementDate(LocalDate.now());
        payment.setClearingSystemId("QCB");
        payment.setPurpose("1");
        payment.setReceivingPSP("QISBQAQA");
        payment.setSendingPSP("QMAGQAQAMPC");
        payment.setCreditingCustomerAccount("QIBA001M0097412234500");
        payment.setCreditingBank(messageType.crediting("QISBQAQA", "QMAGQAQAMPC"));
        payment.setDebitingCustomerAccount("barwa001M0097466426636");
        payment.setDebitingBank(messageType.debiting("QMAGQAQAMPC", "QISBQAQA"));
        return payment;
    }


}
