package com.progressoft.mpay.services;

import com.progressoft.mpay.controllers.ControllerSettings;
import com.progressoft.mpay.controllers.messages.MessageController;
import com.progressoft.mpay.controllers.mpclear.MPClearController;
import com.progressoft.mpay.daos.IDaoFactory;
import com.progressoft.mpay.daos.impl.daos.DaoFactory;
import com.progressoft.mpay.models.IModelFactory;
import com.progressoft.mpay.models.impl.ModelFactoryImpl;
import com.progressoft.mpay.usecases.impl.UseCaseFactoryImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class MPayConfig {
    @Value("${system.tenant}")
    private String systemTenant;
    @Value("${system.orgId}")
    private long systemOrgId;
    @Value("${mpclear.processor}")
    private String mpclearProcessor;

    @Bean
    @Primary
    public IDaoFactory getDaoFactory() {
        return new DaoFactory();
    }

    @Bean
    public ControllerSettings getSettings() {
        ControllerSettings settings = new ControllerSettings();
        settings.setMpclearProcessor(mpclearProcessor);
        settings.setSystemTenant(systemTenant);
        settings.setSystemOrgId(systemOrgId);
        return settings;
    }

    @Bean
    @Primary
    public IModelFactory getModelFactory(IDaoFactory daoFactory) {
        return new ModelFactoryImpl(daoFactory);
    }

    @Bean
    public MessageController getMessageController(IModelFactory modelFactory) {
        return new MessageController(new UseCaseFactoryImpl(modelFactory), modelFactory, getSettings());
    }

    @Bean
    public MPClearController getMPClearController() {
        ModelFactoryImpl modelFactory = new ModelFactoryImpl(getDaoFactory());
        return new MPClearController(new UseCaseFactoryImpl(modelFactory), modelFactory, getSettings());
    }
}
