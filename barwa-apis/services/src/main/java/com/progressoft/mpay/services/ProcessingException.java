package com.progressoft.mpay.services;

import com.progressoft.mpay.entities.CoreResponse;

public class ProcessingException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private CoreResponse coreResponse;

	public ProcessingException() {
	}

	public ProcessingException(CoreResponse coreResponse) {
		this.setCoreResponse(coreResponse);
	}

	public CoreResponse getCoreResponse() {
		return coreResponse;
	}

	public void setCoreResponse(CoreResponse coreResponse) {
		this.coreResponse = coreResponse;
	}
}
