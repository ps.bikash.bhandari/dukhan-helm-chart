package com.progressoft.mpay.services;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.progressoft.mpay.models.IModelFactory;

@Component
@Order(1)
public class MPayWebFilter implements Filter {

    @Value("${system.webservice.displayspecs}")
    private String displaySpecs;
    @Autowired
    private IModelFactory modelFactory;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, javax.servlet.FilterChain chain) throws IOException, ServletException {
        boolean displaySpecifications = "1".equals(displaySpecs);
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        if (displaySpecifications) {
            chain.doFilter(request, response);
            modelFactory.getLogModel().debug("Request Received, URL: " + req.getRequestURI() + ", METHOD: " + req.getMethod());
            return;
        }
//        if (!req.getMethod().equals(HttpMethod.POST.name())) {
//            modelFactory.getLogModel().debug("Request Received, URL: " + req.getRequestURI() + ", METHOD: " + req.getMethod());
//            modelFactory.getLogModel().debug("Display Web Services Specifications is disabled");
//            res.sendError(HttpServletResponse.SC_NOT_FOUND);
//            return;
//        }
        chain.doFilter(request, response);
        modelFactory.getLogModel().debug("Request Received, URL: " + req.getRequestURI() + ", METHOD: " + req.getMethod());
    }
}
