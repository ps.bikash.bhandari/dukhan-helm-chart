package com.progressoft.mpay.services.heartbeat.scheduler;

import com.progressoft.mpay.daos.IDaoFactory;
import com.progressoft.mpay.models.impl.ModelFactoryImpl;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
public class DBCheckScheduler {

    private final AtomicInteger gauge;
    private final IDaoFactory iDaoFactory;

    public DBCheckScheduler(MeterRegistry meterRegistry, IDaoFactory iDaoFactory) {
        gauge = new AtomicInteger();
        this.iDaoFactory = iDaoFactory;
        meterRegistry.gauge("db_status", gauge);
    }

    @Scheduled(fixedRateString = "${app.dbcheck.interval}")
    public void schedulingTask() {
        try {
            new ModelFactoryImpl(iDaoFactory).getCheckConnectionModel().checkConnection();
            gauge.set(1);
        } catch (Exception e) {
            gauge.set(0);
            throw e;
        }
    }

    public AtomicInteger getGauge() {
        return gauge;
    }
}