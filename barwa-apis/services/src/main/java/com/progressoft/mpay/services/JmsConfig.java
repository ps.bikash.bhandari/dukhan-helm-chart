package com.progressoft.mpay.services;

import com.progressoft.mpay.common.MplcearAqtiveMq;
import com.progressoft.mpay.controllers.mpclear.MPClearController;
import com.progressoft.mpay.controllers.mpclear.MPClearControllerEnvelop;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.QosSettings;

@Configuration
public class JmsConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(JmsConfig.class);

    @Autowired
    MPClearController mpclearController;
    @Value("${spring.activemq.brokerurl}")
    private String activeMqUrl;
    @Value("${spring.activemq.user}")
    private String userName;
    @Value("${spring.activemq.password}")
    private String password;


    @Bean(name = "mpClearFactory")
    @Primary
    public ActiveMQConnectionFactory connectionFactoryWithMpClear() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(activeMqUrl);
        connectionFactory.setPassword(password);
        connectionFactory.setUserName(userName);
        return connectionFactory;
    }

    @Bean(name = "jmsTemplateMpClear")
    @Primary
    public JmsTemplate jmsTemplateForMpClear() {
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactoryWithMpClear());
        return template;
    }

    @Bean
    public MplcearAqtiveMq getMPCLEARIntegrationUseCase() {
        return new MplcearAqtiveMq(jmsTemplateForMpClear());
    }

    @Bean(name = "jmsListenerForMpClear")
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactoryWithMpClear());
        QosSettings replyQosSettings = new QosSettings();
        replyQosSettings.setTimeToLive(10000);
        factory.setReplyQosSettings(replyQosSettings);
        return factory;
    }

    @JmsListener(destination = "mpc.dukh.reply.inward")
    public void receiveReplyQueue(String message) {
        LOGGER.info("Response Received: " + message);
        String tenant = "BARWA";
        MPClearControllerEnvelop envelop = new MPClearControllerEnvelop();
        envelop.setContent(message);
        envelop.setTenant(tenant);
        envelop.setOrgId(TenantsLoader.getInstance().getOrgId(tenant));
        mpclearController.processMessage(envelop);
    }

    @JmsListener(destination = "mpc.dukh.payment.inward")
    public void receivePaymentQueue(String message) {
        LOGGER.info("Request Received: " + message);
        String tenant = "BARWA";
        MPClearControllerEnvelop envelop = new MPClearControllerEnvelop();
        envelop.setContent(message);
        envelop.setTenant(tenant);
        envelop.setOrgId(TenantsLoader.getInstance().getOrgId(tenant));
        mpclearController.processMessage(envelop);
    }


}