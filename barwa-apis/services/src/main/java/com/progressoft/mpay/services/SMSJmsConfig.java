package com.progressoft.mpay.services;

import com.google.gson.Gson;
import com.progressoft.mpay.common.MPayActiveMq;
import com.progressoft.mpay.barwa.plugins.smsservice.context.SMSNotificationContext;
import com.progressoft.mpay.barwa.plugins.smsservice.response.SMSNotificationResponse;
import com.progressoft.mpay.barwa.plugins.smsservice.service.SMSNotificationService;
import com.progressoft.mpay.models.IModelFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.QosSettings;

@Configuration
public class SMSJmsConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(SMSJmsConfig.class);

    IModelFactory iModelFactory;

    @Value("${spring.activemq.brokerUrl.local}")
    private String activeMqUrl;
    @Value("${spring.activemq.user.local}")
    private String userName;
    @Value("${spring.activemq.password.local}")
    private String password;

    public SMSJmsConfig(IModelFactory iModelFactory) {
        this.iModelFactory = iModelFactory;
    }

    @Bean(name = "localFactory")
    public ActiveMQConnectionFactory connectionFactoryWithMPay() {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(activeMqUrl);
        connectionFactory.setPassword(password);
        connectionFactory.setUserName(userName);
        return connectionFactory;
    }

    @Bean(name = "jmsTemplateMPay")
    public JmsTemplate jmsTemplateForMPay() {
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactoryWithMPay());
        return template;
    }

    @Bean
    public MPayActiveMq getMPayActiveMQ() {
        return new MPayActiveMq(jmsTemplateForMPay());
    }

    @Bean(name = "jmsListenerForMPay")
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactoryWithMPay());
        QosSettings replyQosSettings = new QosSettings();
        replyQosSettings.setTimeToLive(10000);
        factory.setReplyQosSettings(replyQosSettings);
        return factory;
    }

    @JmsListener(destination = "mpay.barwa.sms.outward", containerFactory = "jmsListenerForMPay")
    public void receiveSMS(String message) {
        LOGGER.info("Sms Received " + message);
        SMSNotificationContext context = new Gson().fromJson(message, SMSNotificationContext.class);
        SMSNotificationResponse response = SMSNotificationService.integrate(context, iModelFactory);
        if (response != null && response.isSuccess() != null && response.isSuccess())
            LOGGER.info("SUCCESS send sms for :" + context.getMobileNumber());
        else
            LOGGER.info("FAIL send sms for :" + context.getMobileNumber());
    }


}