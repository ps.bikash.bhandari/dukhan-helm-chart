package com.progressoft.mpay.services;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import com.progressoft.mpay.exceptions.GenericException;

public class TenantsLoader {
	private static TenantsLoader instance;
	private Map<String, Long> tenants;

	private TenantsLoader() {
		initialize();
	}

	public static TenantsLoader getInstance() {
		if (instance == null)
			instance = new TenantsLoader();
		return instance;
	}

	private void initialize() {
		this.tenants = new ConcurrentHashMap<>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		try {
			Document document = factory.newDocumentBuilder().parse(classLoader.getResourceAsStream("Tenants.xml"));
			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expression = xpath.compile("/Tenants/Tenant");
			NodeList tenantsNodes = (NodeList) expression.evaluate(document, XPathConstants.NODESET);
			for (int i = 0; i < tenantsNodes.getLength(); i++) {
				NamedNodeMap attributes = tenantsNodes.item(i).getAttributes();
				tenants.put(attributes.getNamedItem("Code").getTextContent(), Long.parseLong(attributes.getNamedItem("OrgId").getTextContent()));
			}
		} catch (Exception e) {
			throw new GenericException("Failed to load tenants", e);
		}
	}

	public Map<String, Long> getTenants() {
		return tenants;
	}
	
	public long getOrgId(String tenantCode) {
		return tenants.get(tenantCode);
	}
}