package com.progressoft.mpay.services.messageprocessing;

import com.progressoft.mpay.entities.CoreRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.progressoft.mpay.common.ProcessingStatuses;
import com.progressoft.mpay.common.Reasons;
import com.progressoft.mpay.common.SystemConfigurationKeys;
import com.progressoft.mpay.controllers.messages.MessageController;
import com.progressoft.mpay.controllers.messages.MessageControllerEnvelop;
import com.progressoft.mpay.controllers.messages.MessageControllerResponse;
import com.progressoft.mpay.controllers.mpclear.MPClearController;
import com.progressoft.mpay.controllers.mpclear.MPClearControllerEnvelop;
import com.progressoft.mpay.controllers.mpclear.MPClearControllerResponse;
import com.progressoft.mpay.entities.CoreResponse;
import com.progressoft.mpay.entities.CoreResponseEnvelop;
import com.progressoft.mpay.entities.Reason;
import com.progressoft.mpay.models.IModelFactory;
import com.progressoft.mpay.services.ProcessingException;
import com.progressoft.mpay.services.TenantsLoader;
import com.progressoft.mpay.usecases.messages.IntegrationProcessingRequest;

@RestController
public class ProcessMessageController {
    @Autowired
    private MessageController messageController;
    @Autowired
    private MPClearController mpclearController;
    @Autowired
    private IModelFactory modelFactory;
    @Value("${system.tenant}")
    private String systemTenant;

    @Value("${application.tenant}")
    private String applicationTenant;

    private static final String UNIQUE_CONSTRAINT = "unique constraint";

    @Transactional
    @PostMapping("/processMessage")
    public String processMessage(@RequestBody String content, @RequestParam("token") String token) {
        return process(content, content, token);
    }

    public String process(String mappedContent, String originalContent, String token) {
        String response;
        MessageControllerResponse controllerResponse;
        modelFactory.getLogModel().debug("--------------------------------------------------------------------------------------------------------");
        modelFactory.getLogModel().debug("Request Received:");
        modelFactory.getLogModel().debug("Request Content:");
        modelFactory.getLogModel().debug(originalContent);
        modelFactory.getLogModel().debug("Token:");
        modelFactory.getLogModel().debug(token);
        controllerResponse = handleMessage(mappedContent, originalContent, token);
        response = getResponseString(getEnvelop(controllerResponse.getResponse(), controllerResponse.getTenant()));
        modelFactory.getLogModel().debug("Response Returned:");
        modelFactory.getLogModel().debug(response);
        return response;
    }


    private CoreResponse generateResponse(Reasons reasonCode) {
        CoreResponse response = new CoreResponse();
        response.setStatusCode(String.valueOf(ProcessingStatuses.REJECTED.getValue()));
        Reason reason = modelFactory.getLookupsModel().get(reasonCode.getValue(), Reason.class);
        response.setErrorCd(reason.getCode());
        response.setDesc(reason.getDescription());
        return response;
    }

    private MessageControllerResponse handleMessage(String mappedContent, String originalContent, String token) {
        try {
            MessageControllerEnvelop envelop = new MessageControllerEnvelop();
            envelop.setMessageContent(mappedContent);
            envelop.setOriginalMessageContent(originalContent);
            envelop.setToken(token);
            envelop.getTenants().putAll(TenantsLoader.getInstance().getTenants());
            MessageControllerResponse  controllerResponse = messageController.processMessage(envelop);
            CoreResponse coreResponse = controllerResponse.getResponse();
            if (controllerResponse.getMpClearResponse() != null) {
                MPClearControllerResponse mpClearControllerResponse = handleMPClear(controllerResponse);
                if (mpClearControllerResponse.getCoreResponse() != null)
                    coreResponse = mpClearControllerResponse.getCoreResponse();
            }
            controllerResponse.setResponse(coreResponse);
            return controllerResponse;
        } catch (Exception e) {
            modelFactory.getLogModel().error(e);
            throw e;
        }
    }

    private CoreResponseEnvelop getEnvelop(CoreResponse response, String tenant) {
        boolean isTokenEncryptionEnabled = modelFactory.getSystemConfigurationModel().getBoolean(SystemConfigurationKeys.ENABLE_TOKEN_ENCRYPTION.getValue(), tenant);
        String hashingAlgorithm = modelFactory.getSystemConfigurationModel().get(SystemConfigurationKeys.HASHING_ALGORTHIM.getValue(), tenant).getValue();
        String encoding = modelFactory.getSystemConfigurationModel().get(SystemConfigurationKeys.DEFAULT_ENCODING.getValue(), tenant).getValue();
        String token = modelFactory.getSecurityModel().hash(modelFactory.getCoreResponseModel().toValues(response), hashingAlgorithm, encoding);
        String keyStorePassword = modelFactory.getSystemConfigurationModel().get(SystemConfigurationKeys.KEY_STORE_PASS.getValue(), tenant).getValue();
        String certificateAlias = modelFactory.getSystemConfigurationModel().get(SystemConfigurationKeys.PSP_CERTIFICATE_ALIAS.getValue(), tenant).getValue();
        if (isTokenEncryptionEnabled)
            token = modelFactory.getSecurityModel().encrypt(token, encoding, keyStorePassword, certificateAlias, true);
        return new CoreResponseEnvelop(response, token);
    }

    private String getResponseString(CoreResponseEnvelop envelop) {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(envelop);
    }

    private MPClearControllerResponse handleMPClear(MessageControllerResponse responseEnvelop) {
        MPClearControllerEnvelop mpClearControllerEnvelop = new MPClearControllerEnvelop();
        mpClearControllerEnvelop.setContent(responseEnvelop.getMpClearResponse());
        mpClearControllerEnvelop.setIntegrationProcessingRequest(new IntegrationProcessingRequest(responseEnvelop.getProcessingRequest()));
        mpClearControllerEnvelop.getIntegrationProcessingRequest().setOriginalMPclearMessage(responseEnvelop.getMpClearRequestMessage());
        mpClearControllerEnvelop.setTenant(responseEnvelop.getTenant());
        mpClearControllerEnvelop.setOrgId(responseEnvelop.getOrgId());
        return mpclearController.processMessage(mpClearControllerEnvelop);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.OK)
    String handleConstraintViolationException(Exception e) {
        if (e.getCause() != null && e.getCause().getCause().getMessage().contains(UNIQUE_CONSTRAINT))
            return getResponseString(getEnvelop(generateResponse(Reasons.MESSAGE_IS_DUPLICATED), applicationTenant));
        return new Gson().toJson(generateResponse(Reasons.INTERNAL_SYSTEM_ERROR));
    }
}
