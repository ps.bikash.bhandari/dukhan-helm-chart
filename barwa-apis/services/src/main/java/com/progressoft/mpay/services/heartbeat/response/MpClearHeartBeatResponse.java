package com.progressoft.mpay.services.heartbeat.response;

public class MpClearHeartBeatResponse {
    private boolean isConnected;
    private String message ;

    public MpClearHeartBeatResponse(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public MpClearHeartBeatResponse(boolean isConnected, String message) {
        this.isConnected = isConnected;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }
}
